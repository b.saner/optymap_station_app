import { Component, OnInit } from '@angular/core';
import {Gazoil} from "../model/gazoil";
import {Gaz} from "../model/gaz";
import {Router} from "@angular/router";
import {DetailstationService} from "../services/detailstation/detailstation.service";

@Component({
  selector: 'app-gaz',
  templateUrl: './gaz.page.html',
  styleUrls: ['./gaz.page.scss'],
})
export class GazPage implements OnInit {
  public id: number;
  public gaz: Gaz;

  constructor(private router: Router,private stationservice: DetailstationService) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      idgaz: number;
    };
    if (state == null || state.idgaz === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      this.id = state.idgaz;
      console.log(this.id);
    }
  }

  ngOnInit() {
    this.detailgaz();
  }
  detailgaz(){
    this.stationservice.gazbyid(this.id).subscribe(text => {
      console.log(text);
      this.gaz = text;
    });
  }
}
