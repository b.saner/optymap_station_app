import { Component } from '@angular/core';
import {MenuService} from './services/menu.service';
import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic/ngx';
import Swal from 'sweetalert2';

import {Platform} from '@ionic/angular';
import {UserService} from "./services/user/user.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public typeuser = localStorage.getItem('typeuser');


public appPages = [
  { title: 'parametrage', url: '/setting', icon: 'build' },
  { title: 'FAQS', url: '/faq', icon: 'recording' },
  { title: 'Condition', url: '/term', icon: 'reader' },
  { title: 'confidentialite', url: '/privacy', icon: 'cube' },
  { title: 'Support', url: '/support', icon: 'help-circle' },
];
  private logged=false;
  private language: string;
  constructor(private globalMenuService: MenuService, private translateService: TranslateService,
              private userservice: UserService ,private fcm: FCM, private platform: Platform) {
    if (localStorage.getItem('lang') ) {
      this.language = (localStorage.getItem('lang'));
      this.translateService.use(this.language);
      console.log(this.language);
    }
    this.initializeApp();

  }
//   linkdisponible(){
//     // eslint-disable-next-line eqeqeq
//     if (this.typeuser == '1'){
//       this.appPages = [
//         { title: 'Se connecter / S\'inscrire', url: '/login', icon: 'people' },
//         { title: 'Parametrage', url: '/setting', icon: 'build' },
//         { title: 'FAQS', url: '/faq', icon: 'recording' },
//         { title: 'Conditions', url: '/term', icon: 'reader' },
//         { title: 'confidentialité', url: '/privacy', icon: 'cube' },
//         { title: 'Support', url: '/support', icon: 'help-circle' },
//       ];
//     }
// else {
//       this.appPages = [
//         { title: 'Mon compte', url: '/compte', icon: 'person' },
//         { title: 'Parametrage', url: '/setting', icon: 'build' },
//         { title: 'FAQS', url: '/faq', icon: 'recording' },
//         { title: 'Conditions', url: '/term', icon: 'reader' },
//         { title: 'confidentialité', url: '/privacy', icon: 'cube' },
//         { title: 'Support', url: '/support', icon: 'help-circle' },
//       ];
//     }
//       }
  initializeApp(){
    this.platform.ready().then(() => {
      if (localStorage.getItem('mdsombre') == '1'){
        document.body.setAttribute('color-theme','dark');
      }
      else {
        document.body.setAttribute('color-theme','light');
      }

      this.fcm.getToken().then(token => {
        console.log(token);
        localStorage.setItem('notification_id', token);
      });

      this.fcm.onNotification().subscribe(data => {
        if (data.wasTapped) {
          console.log('Received in background');
          console.log(data);
          console.log(data.body);
          console.log(data.title);
          Swal.fire({
            icon: 'error',
            title: data.title,
            html: '<p style="font-size: 16px">' + data.body + ' </p>',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,

          });
        } else {
          console.log('Received in foreground');
          console.log(data);
          console.log(data.body);
          console.log(data.title);
          Swal.fire({
            icon: 'error',
            title: data.title,
            html: '<p style="font-size: 16px">' + data.body + ' </p>',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,

          });

        }
      });
    })
    this.globalMenuService.getObservable().subscribe((data) => {

      console.log('Data received', data);
      // eslint-disable-next-line eqeqeq
      if (data.logged == 'true'){
        this.logged = true;
        this.appPages = [
          { title: 'mcompte', url: '/myaccount', icon: 'person' },
          { title: 'parametrage', url: '/setting', icon: 'build' },
          { title: 'FAQS', url: '/faq', icon: 'recording' },
          { title: 'condition', url: '/term', icon: 'reader' },
          { title: 'confidentialite', url: '/privacy', icon: 'cube' },
          { title: 'Support', url: '/support', icon: 'help-circle' },
        ];      }
      else {
        this.logged = false;
        this.appPages = [
          { title: 'parametrage', url: '/setting', icon: 'build' },
          { title: 'FAQS', url: '/faq', icon: 'recording' },
          { title: 'condition', url: '/term', icon: 'reader' },
          { title: 'confidentialite', url: '/privacy', icon: 'cube' },
          { title: 'Support', url: '/support', icon: 'help-circle' },
        ];
      }
    });
  };

  logout() {
    this.userservice.logout();
  }
}
