import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {DetailstationService} from '../services/detailstation/detailstation.service';
import {Superette} from '../model/superette';

@Component({
  selector: 'app-e-money',
  templateUrl: './e-money.page.html',
  styleUrls: ['./e-money.page.scss'],
})
export class EMoneyPage implements OnInit {

  public id: number;
  public type: string;
  public emoney: Superette;

  constructor(private router: Router, private stationservice: DetailstationService) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      idemoney: number;
      type: string;
    };
    if (state == null || state.idemoney === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      this.id = state.idemoney;
      this.type = state.type;
      console.log(this.id);
    }
  }

  ngOnInit() {
    this.detailemoney();
  }
  detailemoney(){
    this.stationservice.superettebyid(this.id).subscribe(text => {
      console.log(text);
      this.emoney = text;
    });
  }
}
