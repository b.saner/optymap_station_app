import {AfterViewChecked, Component, ElementRef, OnDestroy, OnInit, Output, EventEmitter, ViewChild} from '@angular/core';
import {Map, tileLayer, marker, icon, circle, polygon, geoJSON, Marker} from 'leaflet';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
// import * as L from 'leaflet';
declare const L: any;
import * as L1 from 'leaflet.markercluster';
import  'leaflet.locatecontrol';
// import  'leaflet-rotate-map';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import {NavigationExtras, Router} from '@angular/router';
import {ActionSheetController, MenuController, ModalController, Platform} from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import {StationService} from '../services/station/station.service';
import {fromEvent} from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import {UserService} from '../services/user/user.service';
import {MecanicienService} from '../services/mecanicien/mecanicien.service';
import {ReviewService} from '../services/review/review.service';
import {OtherService} from '../services/otherservices/other.service';
import {MaponePage} from '../mapone/mapone.page';
import {DetailpopupPage} from '../detailpopup/detailpopup.page';

import * as mapboxgl from 'mapbox-gl';
import {environment} from '../../environments/environment';
import Swal from 'sweetalert2';
import {MapsettingPage} from '../mapsetting/mapsetting.page';
import {DatePipe} from '@angular/common';
import {MenuService} from '../services/menu.service';


const meicon = L.icon({
  iconUrl: '../assets/icon/station.png',
  iconSize: [35, 41],
  iconAnchor: [10, 41],
  popupAnchor: [2, -45], });

const arreticon = L.icon({
  iconUrl: '../assets/icon/arret.png',
  iconSize: [35, 41],
  iconAnchor: [10, 41],
  popupAnchor: [2, -45], });

const pharmaicon = L.icon({
  iconUrl: '../assets/icon/pharmacie.png',
  iconSize: [35, 41],
  iconAnchor: [10, 41],
  popupAnchor: [2, -45], });
const policeicon = L.icon({
  iconUrl: '../assets/icon/policemark.png',
  iconSize: [35, 41],
  iconAnchor: [10, 41],
  popupAnchor: [2, -45], });

const signicon = L.icon({
  iconUrl: '../assets/icon/signalisation.png',
  iconSize: [35, 41],
  iconAnchor: [10, 41],
  popupAnchor: [2, -45], });

const mecaicon = L.icon({
  iconUrl: '../assets/icon/mecanicien.png',
  iconSize: [35, 41],
  iconAnchor: [10, 41],
  popupAnchor: [2, -45], });

const man = L.icon({
  iconUrl: '../assets/my-car.png',
  iconSize: [35, 35],
  iconAnchor: [10, 35],
  popupAnchor: [2, -45], });

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {

  map2: mapboxgl.Map;
  // direction: MapboxD;
  style = 'mapbox://styles/mapbox/navigation-day-v1';
  latbox = 14.71613540624325;
  lngbox = -17.470863834282337;
  @ViewChild('searchInput', {read: ElementRef}) searchInput: ElementRef;

  map: Map;
  mecaMarkers: any;
  signaMarkers: any;
  pharmaMarkers: any;
  newMarkers: any;
  public currentStation: any = [];
  public currentMecanicien: any = [];
  public currentArret: any = [];
  public currentHopital: any = [];
  public currentVulcanisateur: any = [];
  public currentPharmacie: any = [];
  public currentOrange: any = [];
  public currentGendarmerie: any = [];
  public currentSignalisation: any = [];

  public latlong: any;
  public locations: any;
  public search: any;
  public markergroup: L1.MarkerClusterGroup;
  public lc: any;
  public a = false;
  public ratingnumb: number;
  public text = 'Déplacez vous partout au Sénégal et decouvrez les services qui vous entourent';
  public imgurl = 'https://cdn.pixabay.com/photo/2019/12/26/05/10/pink-4719682_960_720.jpg';
  public link = 'https://link.medium.com/JA4amAHFJ5';

  public datenow: Date = new Date();

  private profile = 'mapbox/cycling';
  private locationmecanicien: any;
  private locationsignalisation: any;
  private locationpharmacie: any;
  private locationorange: any;
  private locationgendarmerie: any;
  private locationarret: any;
  private locationhopital: any;
  private locationvulcanisateur: any;
  private markerposition = new mapboxgl.Marker();
  // private directions = new MapboxDirections({
  //   accessToken: environment.mapbox.accessToken,
  //   unit: 'metric',
  //   profile:this.profile
  // });
  private mapsharelink: string;
  private geolocate: mapboxgl.GeolocateControl;
  private enableposition: boolean;

  constructor(private geolocation: Geolocation, public modalController: ModalController,
              private router: Router, private socialSharing: SocialSharing, private mapservice: StationService,
              private mecanicienservice: MecanicienService, private reviewService: ReviewService,
              private speechRecognition: SpeechRecognition, private platform: Platform, private authService: UserService,
              private otherService: OtherService, private menuController: MenuController,
              public actionSheetController: ActionSheetController,private datePipe: DatePipe,
              private globalMenuService: MenuService) {
    this.iswelcome();

  }

  ngOnInit() {
    if (19 <= parseInt(this.datePipe.transform(this.datenow, 'HH'))
      || parseInt(this.datePipe.transform(this.datenow, 'HH')) <= 7
    ){
      console.log('nuit');
      this.style = 'mapbox://styles/mapbox/navigation-night-v1';

    }
    else if (localStorage.getItem('mapsetting')) {
      this.style = (localStorage.getItem('mapsetting'));
    }
    else{
      console.log('jour');
      this.style = 'mapbox://styles/mapbox/navigation-day-v1';

    }
      // Check permission vocal
    this.speechRecognition.hasPermission()
      .then((hasPermission: boolean) => {
        console.log(hasPermission);
        if (hasPermission === false) {
// Request permissions
          this.speechRecognition.requestPermission()
            .then(
              () => console.log('Granted'),
              () => console.log('Denied')
            );
        }
      });

    // @ts-ignore
    mapboxgl.accessToken = environment.mapbox.accessToken;

    this.map2 = new mapboxgl.Map({
      container: 'mapId',
      style: this.style,
      zoom: 8,
      center: [this.lngbox, this.latbox],
      doubleClickZoom: false,

    });
    this.geolocate = new mapboxgl.GeolocateControl({
      positionOptions: {
        enableHighAccuracy: true
      },
      showUserHeading: true,
      trackUserLocation: true,
    });
    this.map2.on('dblclick', this.addmarker.bind(this));
    this.map2.on('contextmenu', this.addmarker.bind(this));

  }

  ionViewDidEnter() {
    // this.playwolof();
    setTimeout(() => this.map2.resize(), 0);

    // this.locatemapbox();

    console.log('enter');


    // this.map2.on('load', () => {
    //   this.geolocate.trigger();
    // });
    // Add map controls
    // this.map2.addControl(new mapboxgl.NavigationControl()
    // );
    this.map2.addControl(this.geolocate,'bottom-left');
    // request location update and set location
    // this.map = new Map('mapId');
    // tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
    // ).addTo(this.map);
    // // }
    // this.locatePosition();
    // this.loadpositions();
    // this.loadmecanicien();
    // this.loadsignalisation();
    // this.loadpharmacie();
    // this.loadarret();

    fromEvent<any>(this.searchInput.nativeElement, 'keyup')
      .pipe(
        map(ev => ev.target.value), debounceTime(1500)
      );
    // .subscribe(term => {
    //
    //   console.log(term);
    //   console.log(term.length);
    //   if (term.length > 0) {
    //     this.mapservice.searchstation(term).subscribe(stationsearch => {
    //       for (let i = 0; i < this.mapMarkers.length; i++) {
    //         this.map.removeLayer(this.mapMarkers[i]);
    //       }
    //       this.mapMarkers = [];
    //       this.locations = stationsearch;
    //       // console.log(this.locations);
    //       // console.log(this.locations.length);
    //       for (const val of this.locations) {
    //         console.log(val); // prints values: 10, 20, 30, 40
    //         const idstation = val.id;
    //         const navigationExtras: NavigationExtras = {
    //           state: {idstation}
    //         };
    //         this.newMarkers = marker([val.latitude, val.longitude], {
    //           draggable: false,
    //           icon: meicon,
    //         }).addTo(this.map)
    //           .bindPopup(val.nom)
    //           // tslint:disable-next-line:only-arrow-functions
    //           .on('click', event => {
    //             this.router.navigate(['/detailstation'], navigationExtras);
    //           });
    //         this.mapMarkers.push(this.newMarkers);
    //       }
    //     });
    //   } else {
    //     this.loadpositions();
    //   }
    // });

  }

  locatemapbox() {
    this.geolocation.getCurrentPosition().then((resp) => {
      this.latbox = resp.coords.latitude;
      this.lngbox = resp.coords.longitude;
      console.log(resp);
    }).catch((error) => {
      console.log('Error getting location', error);
    });
    // this.map2.on('load',()=> {
    //   this.map2.flyTo({
    //     center: [this.lngbox,this.latbox],
    //   essential: true // this animation is considered essential with respect to prefers-reduced-motion
    // });
    // });
  }

  locatePosition() {

    this.lc = L.control.locate({
      locateOptions: {
        enableHighAccuracy: true,
        flyTo: true,
        showPopup: false
      }
    })
      .addTo(this.map);

// request location update and set location
    this.map.locate({
      watch: false,
      setView: true,
      maxZoom: 12,
      enableHighAccuracy: true
    }).on('locationfound', (e: any) => {
      console.log('location', e);
      this.latlong = e.latlng;
    });
    //         const navigationExtras: NavigationExtras = {
    //         state: {id : "1" }
    //        };
    //      Si la localisation est disponible on integrera notre position
    //     this.newMarker = marker([e.latitude, e.longitude], {
    //       draggable: false,
    //       icon: man
    //     })
    //       .on('click', event =>  {
    //       this.router.navigate(['/detailstation'], navigationExtras);
    //     })
    //     .addTo(this.map);
    //      this.newMarker.bindPopup('<p>Ma position</p>');
    //      this.newMarker2 = marker([e.latitude, e.longitude], animatedCircleIcon).addTo(this.map);
    //    });
    //
    // });
  }

  locatePositionpres() {
    if (this.a === false) {
      this.map.setView(this.latlong, 13);
      this.lc.start();
      this.a = true;
    } else {
      this.lc.stop();
      this.a = false;
    }

    //   this.map.locate().on('locationfound', (e: any) => {
    //   console.log('location', e);
    //   // Si la localisation est disponible on integrera notre position
    //   // this.newMarker = marker([e.latitude, e.longitude], {
    //   //   draggable: false,
    //   //   icon: meicon,
    //   //
    //   // }).addTo(this.map);
    //   // this.newMarker.bindPopup('<p>Ma position</p>');
    // });
  }


  // loadpositions() {
  //   this.mapservice.allstation().subscribe(positions => {
  //     this.locations = positions;
  //     console.log(this.locations);
  //     console.log(this.locations.length);
  //     for (const val of this.locations) {
  //       const idstation = val.id;
  //       const navigationExtras: NavigationExtras = {
  //         state: {idstation}
  //       };
  //       this.newMarkers = L.marker([val.latitude, val.longitude], {
  //         draggable: false,
  //         icon: meicon,
  //       }).addTo(this.map2)
  //         // .bindPopup('<p class="test" [routerLink]="[detailstation]" [state]="{idstation:' + idstation + '}">' + val.nom + '<p>')
  //         // tslint:disable-next-line:only-arrow-functions
  //         .on('click', event => {
  //           this.infopop(idstation,'station');
  //           console.log('click fait');
  //           // this.router.navigate(['/detailstation'], navigationExtras);
  //         });
  //       this.mapMarkers.push(this.newMarkers);
  //
  //     }
  //
  //     // tslint:disable-next-line:variable-name
  //     // const marker_group  = new L1.MarkerClusterGroup({
  //     //   animation : true
  //     // });
  //     // for (const val of positions) {
  //     //   console.log(val); // prints values: 10, 20, 30, 40
  //     //   const idsignalisation = val.id;
  //     //   const navigationExtras: NavigationExtras = {
  //     //     state: {idsignalisation }
  //     //   };
  //     //   marker_group.addLayer(L.marker([val.latitude, val.longitude]))
  //     //
  //     //   // .bindPopup(val.motif)
  //     //
  //     //   // tslint:disable-next-line:only-arrow-functions
  //     //     .on('click', event =>  {
  //     //       // this.router.navigate(['/signalisationdetail'], navigationExtras);
  //     //     });
  //     //   this.map.addLayer(marker_group);
  //     //
  //     // }
  //
  //   });
  //
  // }

  gotodetail(id) {
    const idstation = id;
    const navigationExtras: NavigationExtras = {
      state: {idstation}
    };
    this.router.navigate(['/detailstation'], navigationExtras);

  }

// Check if sharing via email is supported
  shareemail() {
    this.socialSharing.canShareViaEmail().then(() => {
      // Sharing via email is possible
    }).catch(() => {
      // Sharing via email is not possible
    });
  }

// Share via email
  sharee() {
    this.socialSharing.shareViaEmail('Body', 'Sunustation', ['recipient@example.org']).then(() => {
      // Success!
    }).catch(() => {
      // Error!
    });
  }

  shareWhatsapp() {
    this.socialSharing.shareViaWhatsApp(this.text);
  }

  shareFacebook() {
    this.socialSharing.shareViaFacebookWithPasteMessageHint(this.text, this.imgurl, null /* url */, 'Copier!');
  }

  sendTwitter() {
    this.socialSharing.shareViaTwitter(this.text);
  }

  sendInstagram() {
    this.socialSharing.shareViaInstagram(this.text, this.imgurl);
  }

  ionViewDidLeave() {

    console.log('map page leave');
    // this.map2.remove();
  }

//   vocal() {
// // Start the recognition process
//     this.speechRecognition.startListening()
//       .subscribe(
//         (matches: string[]) => {
//           console.log(matches);
//           this.search = matches[0];
//           this.mapservice.searchstation(this.search).subscribe(stationsearch => {
//             for (let i = 0; i < this.mapMarkers.length; i++) {
//               this.map.removeLayer(this.mapMarkers[i]);
//             }
//             this.mapMarkers = [];
//             this.locations = stationsearch;
//             console.log(this.locations);
//             console.log(this.locations.length);
//             for (const val of this.locations) {
//               console.log(val); // prints values: 10, 20, 30, 40
//               const idstation = val.id;
//               const navigationExtras: NavigationExtras = {
//                 state: {idstation}
//               };
//               this.newMarkers = marker([val.latitude, val.longitude], {
//                 draggable: false,
//                 icon: meicon,
//               }).addTo(this.map)
//                 .bindPopup(val.nom)
//                 // tslint:disable-next-line:only-arrow-functions
//                 .on('click', event => {
//                   this.router.navigate(['/detailstation'], navigationExtras);
//                 });
//               this.mapMarkers.push(this.newMarkers);
//             }
//           });
//
//         },
//         (onerror) => console.log('error:', onerror)
//       );
//
// // Stop the recognition process (iOS only)
// //    this.speechRecognition.stopListening();
//
//   }

  gotoaddstation() {
    this.router.navigate(['/addstation']);
  }

  gotoaddgaragiste() {
    this.router.navigate(['/addgaragiste']);
  }

  gotoaddpharmacie() {
    this.router.navigate(['/addpharmacie']);
  }

  gotoaddsignalisation() {
    this.router.navigate(['/signaler']);
  }

  gotoittineraire() {
    this.router.navigate(['/itineraire']);
  }

  // allloadposition(){
  //   this.mapservice.allstation().subscribe(positions => {
  //     this.locations = positions;
  //     console.log(positions);
  //     console.log(positions.length);
  //     // for (const val of positions) {
  //     //   console.log(val); // prints values: 10, 20, 30, 40
  //     //   const idsignalisation = val.id;
  //     //   const navigationExtras: NavigationExtras = {
  //     //     state: {idsignalisation }
  //     //   };
  //     //   L.marker([val.latitude, val.longitude])
  //     //       .addTo(this.map)
  //     //       .bindPopup(val.motif)
  //     //
  //     //       // tslint:disable-next-line:only-arrow-functions
  //     //       .on('click', event =>  {
  //     //         this.router.navigate(['/signalisationdetail'], navigationExtras);
  //     //       });
  //     //              }
  //
  //     // tslint:disable-next-line:variable-name
  //     this.markergroup  = new L1.MarkerClusterGroup({
  //       animation : true
  //     });
  //     for (const val of positions) {
  //       console.log(val); // prints values: 10, 20, 30, 40
  //       const idposition = val.id;
  //       const navigationExtras: NavigationExtras = {
  //         state: {idposition }
  //       };
  //       this.markergroup.addLayer(L.marker([val.latitude, val.longitude])
  //       )
  //
  //
  //       // tslint:disable-next-line:only-arrow-functions
  //         .on('click', event =>  {
  //           // this.router.navigate(['/signalisationdetail'], navigationExtras);
  //         });
  //       this.map.addLayer(markergroup);
  //     }
  //   });
  //
  // }

  iswelcome() {
    this.platform.ready().then(() => {
      if (!this.authService.isLoggedIn()) {
        // Swal.fire('Not connected', 'Veuillez vous connecter afin d\'acceder a la ressource demandee!', 'error')
        //     .then(res => {
        //         if (res.value) {
        this.router.navigate(['welcome']);
      }
    });
  }

  loadstation(event) {
    event.stopPropagation();
    this.mapservice.allstation().subscribe(positions => {
      this.locations = positions;
      if (this.currentStation.length == 0) {
        this.clearmarkersignalisation();
        this.clearmarkermecanicien();
        this.clearmarkerarret();
        this.clearmarkerpharmacie();
        this.clearmarkerhopital();
        this.clearmarkervulcanisateur();
        this.clearmarkergendarmerie();
        for (const val of this.locations) {
          const idstation = val.id;

          const station = document.createElement('div');
          station.classList.add('stationpng');
          const marker1 = new mapboxgl.Marker(station)
            .setLngLat([val.longitude, val.latitude])
            .addTo(this.map2);
          const marker1el = marker1.getElement();
          marker1el.addEventListener('click', event => {
            this.infopop(idstation, 'station');
            console.log('click fait');
          });
          this.currentStation.push(marker1el);

        }
      }
      // tslint:disable-next-line:variable-name
      // const marker_group  = new L1.MarkerClusterGroup({
      //   animation : true
      // });
      // for (const val of positions) {
      //   console.log(val); // prints values: 10, 20, 30, 40
      //   const idsignalisation = val.id;
      //   const navigationExtras: NavigationExtras = {
      //     state: {idsignalisation }
      //   };
      //   marker_group.addLayer(L.marker([val.latitude, val.longitude]))
      //
      //   // .bindPopup(val.motif)
      //
      //   // tslint:disable-next-line:only-arrow-functions
      //     .on('click', event =>  {
      //       // this.router.navigate(['/signalisationdetail'], navigationExtras);
      //     });
      //   this.map.addLayer(marker_group);
      //
      // }

    });

  }

  loadmecanicien(event) {
    event.stopPropagation();

    this.mecanicienservice.allmecanicien().subscribe(positions => {
      this.locationmecanicien = positions;
      if (this.currentMecanicien.length == 0) {
        this.clearmarkersignalisation();
        this.clearmarkerstation();
        this.clearmarkerarret();
        this.clearmarkerpharmacie();
        this.clearmarkerorange();
        this.clearmarkerhopital();
        this.clearmarkervulcanisateur();
        this.clearmarkergendarmerie();
        for (const val of this.locationmecanicien) {
          const idmecanicien = val.id;

          const mecanicien = document.createElement('div');
          mecanicien.classList.add('mecanicienpng');
          const marker1 = new mapboxgl.Marker(mecanicien)
            .setLngLat([val.longitude, val.latitude])
            .addTo(this.map2);
          const marker1el = marker1.getElement();
          marker1el.addEventListener('click', event => {
            this.infopop(idmecanicien, 'mecanicien');
          });
          this.currentMecanicien.push(marker1el);

        }
      }


    });
  }

  loadsignalisation(event) {
    event.stopPropagation();

    this.otherService.allsignalisation().subscribe(positions => {
      this.locationsignalisation = positions;
      if (this.currentSignalisation.length == 0) {
        this.clearmarkerstation();
        this.clearmarkermecanicien();
        this.clearmarkerarret();
        this.clearmarkerpharmacie();
        this.clearmarkerorange();
        this.clearmarkerhopital();
        this.clearmarkervulcanisateur();
        this.clearmarkergendarmerie();
        for (const val of this.locationsignalisation) {
          const idsignalisation = val.id;

          const signalisation = document.createElement('div');
          signalisation.classList.add('signalisationpng');
          const marker1 = new mapboxgl.Marker(signalisation)
            .setLngLat([val.longitude, val.latitude])
            .addTo(this.map2);
          const marker1el = marker1.getElement();
          marker1el.addEventListener('click', event => {
            this.infopop(idsignalisation, 'signalisation');
          });
          this.currentMecanicien.push(marker1el);

        }
      }
    });
  }

  loadpharmacie(event) {
    event.stopPropagation();

    this.otherService.allpharmacie().subscribe(positions => {
      this.locationpharmacie = positions;
      if (this.currentPharmacie.length == 0) {
        this.clearmarkersignalisation();
        this.clearmarkermecanicien();
        this.clearmarkerarret();
        this.clearmarkerstation();
        this.clearmarkerorange();
        this.clearmarkerhopital();
        this.clearmarkervulcanisateur();
        this.clearmarkergendarmerie();
        for (const val of this.locationpharmacie) {
          const idpharmacie = val.id;

          const pharmacie = document.createElement('div');
          pharmacie.classList.add('pharmaciepng');
          const marker1 = new mapboxgl.Marker(pharmacie)
            .setLngLat([val.longitude, val.latitude])
            .addTo(this.map2);
          const marker1el = marker1.getElement();
          marker1el.addEventListener('click', event => {
            this.infopop(idpharmacie, 'pharmacie');
            console.log('click fait');
          });
          this.currentPharmacie.push(marker1el);

        }
      }
    });
  }

  loadorange(event) {
    event.stopPropagation();

    this.otherService.allorange().subscribe(positions => {
      this.locationorange = positions;
      if (this.currentOrange.length == 0) {
        this.clearmarkersignalisation();
        this.clearmarkermecanicien();
        this.clearmarkerarret();
        this.clearmarkerstation();
        this.clearmarkerpharmacie();
        this.clearmarkerhopital();
        this.clearmarkervulcanisateur();
        this.clearmarkergendarmerie();
        for (const val of this.locationorange) {
          const idorange = val.id;

          const orange = document.createElement('div');
          orange.classList.add('pharmaciepng');
          const marker1 = new mapboxgl.Marker(orange)
            .setLngLat([val.longitude, val.latitude])
            .addTo(this.map2);
          const marker1el = marker1.getElement();
          marker1el.addEventListener('click', event => {
            this.infopop(idorange, 'orange');
            console.log('click fait');
          });
          this.currentOrange.push(marker1el);

        }
      }
    });
  }

  loadarret(event) {
    event.stopPropagation();

    this.otherService.allarret().subscribe(positions => {
      this.locationarret = positions;
      if (this.currentArret.length == 0) {
        this.clearmarkersignalisation();
        this.clearmarkermecanicien();
        this.clearmarkerstation();
        this.clearmarkerpharmacie();
        this.clearmarkerorange();
        this.clearmarkerhopital();
        this.clearmarkervulcanisateur();
        this.clearmarkergendarmerie();
        for (const val of this.locationarret) {
          const idarret = val.id;
          const navigationExtras: NavigationExtras = {
            state: {idarret}
          };
          const arret = document.createElement('div');
          arret.classList.add('arretpng');
          const marker1 = new mapboxgl.Marker(arret)
            .setLngLat([val.longitude, val.latitude])
            .addTo(this.map2);
          const marker1el = marker1.getElement();
          marker1el.addEventListener('click', event => {
            // this.infopop(idarret, 'arret');
            this.router.navigate(['/arret'], navigationExtras);

            console.log('click fait');
          });
          this.currentArret.push(marker1el);

        }
      }
    });
  }

  loadhopital(event) {
    event.stopPropagation();
    this.otherService.allhopital().subscribe(positions => {
      this.locationhopital = positions;
      if (this.currentHopital.length == 0) {
        this.clearmarkersignalisation();
        this.clearmarkermecanicien();
        this.clearmarkerstation();
        this.clearmarkerpharmacie();
        this.clearmarkerorange();
        this.clearmarkerarret();
        this.clearmarkergendarmerie();
        this.clearmarkervulcanisateur();
        for (const val of this.locationhopital) {
          const idhopital = val.id;
          const navigationExtras: NavigationExtras = {
            state: {idhopital}
          };
          const hopital = document.createElement('div');
          hopital.classList.add('hopitalpng');
          const marker1 = new mapboxgl.Marker(hopital)
            .setLngLat([val.longitude, val.latitude])
            .addTo(this.map2);
          const marker1el = marker1.getElement();
          marker1el.addEventListener('click', event => {
            this.infopop(idhopital, 'hopital');
            // this.router.navigate(['/hopital'], navigationExtras);
            //
            console.log('click fait');
          });
          this.currentHopital.push(marker1el);

        }
      }
    });
  }

  loadvulcanisateur(event) {
    event.stopPropagation();
    this.otherService.allvulcanisateur().subscribe(positions => {
      this.locationvulcanisateur = positions;
      if (this.currentVulcanisateur.length == 0) {
        this.clearmarkersignalisation();
        this.clearmarkermecanicien();
        this.clearmarkerstation();
        this.clearmarkerpharmacie();
        this.clearmarkerorange();
        this.clearmarkerarret();
        this.clearmarkerhopital();
        this.clearmarkergendarmerie();
        for (const val of this.locationvulcanisateur) {
          const idvulcanisateur = val.id;
          const navigationExtras: NavigationExtras = {
            state: {idvulcanisateur}
          };
          const vulcanisateur = document.createElement('div');
          vulcanisateur.classList.add('vulcanicateurpng');
          const marker1 = new mapboxgl.Marker(vulcanisateur)
            .setLngLat([val.longitude, val.latitude])
            .addTo(this.map2);
          const marker1el = marker1.getElement();
          marker1el.addEventListener('click', event => {
            this.infopop(idvulcanisateur, 'vulcanisateur');
            // this.router.navigate(['/vulcanisateur'], navigationExtras);

            console.log('click fait');
          });
          this.currentVulcanisateur.push(marker1el);

        }
      }
    });
  }

  loadgendarme(event) {
    event.stopPropagation();

    this.otherService.allgendarmerie().subscribe(positions => {
      this.locationgendarmerie = positions;
      if (this.currentGendarmerie.length == 0) {
        this.clearmarkersignalisation();
        this.clearmarkermecanicien();
        this.clearmarkerarret();
        this.clearmarkerstation();
        this.clearmarkerpharmacie();
        this.clearmarkerhopital();
        this.clearmarkervulcanisateur();
        this.clearmarkerorange();
        for (const val of this.locationgendarmerie) {
          const idgendarmerie = val.id;

          const gendarmerie = document.createElement('div');
          gendarmerie.classList.add('gendarmepng');
          const marker1 = new mapboxgl.Marker(gendarmerie)
            .setLngLat([val.longitude, val.latitude])
            .addTo(this.map2);
          const marker1el = marker1.getElement();
          marker1el.addEventListener('click', event => {
            this.infopop(idgendarmerie, 'gendarmerie');
            console.log('click fait');
          });
          this.currentGendarmerie.push(marker1el);

        }
      }
    });
  }

  async infopop(id, type) {
    const signalerModal = await this.modalController.create({
      component: DetailpopupPage,
      cssClass: 'cmodal',
      swipeToClose: true, componentProps: {
        id, type
      }
    });
    return await signalerModal.present();

  }

  menu() {
    this.menuController.toggle();
  }

  clearmarkerstation() {
    if (this.currentStation !== null || this.currentStation !== []) {
      for (let i = this.currentStation.length - 1; i >= 0; i--) {
        this.currentStation[i].remove();
      }
      this.currentStation.length = 0;
    }
  }

  clearmarkermecanicien() {
    if (this.currentMecanicien !== null) {
      for (let i = this.currentMecanicien.length - 1; i >= 0; i--) {
        this.currentMecanicien[i].remove();
      }
      this.currentMecanicien.length = 0;
    }
    console.log(this.currentMecanicien);
  }

  clearmarkerarret() {
    console.log(this.currentArret);
    if (this.currentArret !== null || this.currentArret !== []) {
      for (let i = this.currentArret.length - 1; i >= 0; i--) {
        this.currentArret[i].remove();
      }
      this.currentArret.length = 0;

    }
  }

  clearmarkerorange() {
    console.log(this.currentOrange);
    if (this.currentOrange !== null || this.currentOrange !== []) {
      for (let i = this.currentOrange.length - 1; i >= 0; i--) {
        this.currentOrange[i].remove();
      }
      this.currentOrange.length = 0;

    }
  }

  clearmarkergendarmerie() {
    if (this.currentGendarmerie !== null || this.currentGendarmerie !== []) {
      for (let i = this.currentGendarmerie.length - 1; i >= 0; i--) {
        this.currentGendarmerie[i].remove();
      }
      this.currentGendarmerie.length = 0;

    }
  }

  clearmarkerpharmacie() {
    if (this.currentPharmacie !== null || this.currentPharmacie !== []) {
      for (let i = this.currentPharmacie.length - 1; i >= 0; i--) {
        this.currentPharmacie[i].remove();
      }
      this.currentPharmacie.length = 0;
    }
  }

  clearmarkersignalisation() {
    if (this.currentSignalisation !== null || this.currentSignalisation !== []) {
      for (let i = this.currentSignalisation.length - 1; i >= 0; i--) {
        this.currentSignalisation[i].remove();
      }
      this.currentSignalisation.length = 0;
    }
  }

  clearmarkerhopital() {
    if (this.currentHopital !== null || this.currentHopital !== []) {
      for (let i = this.currentHopital.length - 1; i >= 0; i--) {
        this.currentHopital[i].remove();
      }
      this.currentHopital.length = 0;
    }
  }

  clearmarkervulcanisateur() {
    if (this.currentVulcanisateur !== null || this.currentVulcanisateur !== []) {
      for (let i = this.currentVulcanisateur.length - 1; i >= 0; i--) {
        this.currentVulcanisateur[i].remove();
      }
      this.currentVulcanisateur.length = 0;
    }
  }

  addmarker(event) {
    const coordinates = event.lngLat;
    this.markerposition.setLngLat(coordinates).addTo(this.map2);
    this.mapsharelink = 'https://www.google.sn/maps?q=' + coordinates.lat + ',' + coordinates.lng;
    this.presentActionSheet();
    console.log(this.mapsharelink);
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Position',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Partager avec un membre',
        icon: 'share',
        handler: () => {
          console.log('Share clicked');
        }
      }, {
        text: 'Partager en ligne',
        icon: 'share',
        handler: () => {
          console.log('Share clicked');
        }
      },
        {
          text: 'Favoris',
          icon: 'heart',
          handler: () => {
            Swal.fire({
              icon: 'success', title: 'La position a été ajouté en favoris', showConfirmButton: false, timer: 2500,
            });
          }
        }, {
          text: 'Annuler',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
    });
    await actionSheet.present();
    await actionSheet.onDidDismiss().then(() => {
      this.markerposition.remove();
    });
  }

  myposition() {
    this.geolocate.trigger();
    this.geolocate.on('geolocate', (e: any) => {
      console.log('A geolocate event has occurred. ', e);
      this.latbox = e.coords.latitude;
      this.lngbox = e.coords.longitude;
      // this.map2.setCenter([this.lngbox, this.latbox]);
      // this.map2.flyTo({
      //   center:([this.lngbox, this.latbox])
      // });

    });
  }

  async mapsettingp() {
    console.log(this.map2.getStyle().sources);
    const mapsettingModal = await this.modalController.create({
      component: MapsettingPage,
      cssClass: 'cmodalsetting',
      componentProps: {
        styleMap:  this.style,
      },
      swipeToClose: true,
    });
    await mapsettingModal.present();
    this.globalMenuService.mystyleget().subscribe((data) => {
      this.mapstyle(data.value);
      this.style = data.value;
      localStorage.setItem('mapsetting',this.style);
    });


    await mapsettingModal.onDidDismiss().then((data) => {
      console.log(data);
    });

  }

  mapstyle(style: string) {
    this.map2.setStyle(style);
  }

}
