import {Injectable} from '@angular/core';
import {HttpInterceptor, HttpRequest} from '@angular/common/http';
import {
  airederepos,
  apiDev,
  culte,
  departement,
  departementbyregion,
  electrique,
  entretien,
  essence,
  gab,
  gaz,
  gazoil,
  gpl,
  login,
  mecanicien,
  region,
  register,
  rating,
  ratingBymecanicien,
  ratingByuser,
  station,
  superette,
  toilette,
  info,
  pharmacie,
  pharmaciecall,
  signalisation,
  arret,
  line,
  orange,
  vulcanisateur,
  hopital, otpvalidate, gendarmerie,gendarmeriecall
} from '../../../environments/environment';

import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(private jwtHelper: JwtHelperService ) {
    }

    intercept(request: HttpRequest<any>, next) {
        const token = this.jwtHelper.tokenGetter();
        if (token !== null && token !== undefined && !request.url.match(apiDev + login) &&
          !request.url.match(apiDev + register) && !request.url.match(apiDev + rating) &&
          !request.url.match(apiDev + ratingBymecanicien) && !request.url.match(apiDev + ratingByuser) &&
         !request.url.match(apiDev + station) && !request.url.match(apiDev + region) &&
         !request.url.match(apiDev + airederepos) && !request.url.match(apiDev + culte) &&
         !request.url.match(apiDev + electrique) && !request.url.match(apiDev + entretien) &&
         !request.url.match(apiDev + essence) && !request.url.match(apiDev + gab) &&
         !request.url.match(apiDev + gaz) && !request.url.match(apiDev + gazoil) &&
         !request.url.match(apiDev + gpl) && !request.url.match(apiDev + superette) &&
         !request.url.match(apiDev + toilette) && !request.url.match(apiDev + departement) &&
         !request.url.match(apiDev + departementbyregion) && !request.url.match(apiDev + mecanicien) &&
         !request.url.match(apiDev + arret) && !request.url.match(apiDev + line) &&
         !request.url.match(apiDev + vulcanisateur) && !request.url.match(apiDev + hopital) &&
         !request.url.match(apiDev + pharmacie) && !request.url.match(apiDev + pharmaciecall) &&
          !request.url.match(apiDev + gendarmerie) && !request.url.match(apiDev + gendarmeriecall) &&
         !request.url.match(apiDev + signalisation) && !request.url.match(apiDev + otpvalidate) &&
          !request.url.match(apiDev + info) && !request.url.match(apiDev + orange)

        ) {
            request = request.clone({
                setHeaders: {
                    Authorization: 'Bearer ' + token,
                    'Content-type': 'application/json'
                },
            });
        }
        return next.handle(request);
    }
}


