import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddgaragistePage } from './addgaragiste.page';

const routes: Routes = [
  {
    path: '',
    component: AddgaragistePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddgaragistePageRoutingModule {}
