import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddgaragistePageRoutingModule } from './addgaragiste-routing.module';

import { AddgaragistePage } from './addgaragiste.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddgaragistePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [AddgaragistePage]
})
export class AddgaragistePageModule {}
