import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetaillinePageRoutingModule } from './detailline-routing.module';

import { DetaillinePage } from './detailline.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetaillinePageRoutingModule
  ],
  declarations: [DetaillinePage]
})
export class DetaillinePageModule {}
