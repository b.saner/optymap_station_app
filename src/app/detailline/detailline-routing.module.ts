import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetaillinePage } from './detailline.page';

const routes: Routes = [
  {
    path: '',
    component: DetaillinePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetaillinePageRoutingModule {}
