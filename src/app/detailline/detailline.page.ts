import { Component, OnInit } from '@angular/core';
import {Line} from '../model/line';
import {Router} from '@angular/router';
import {CallNumber} from '@ionic-native/call-number/ngx';
import {OtherService} from '../services/otherservices/other.service';
import {ModalController} from '@ionic/angular';
import {ReviewService} from '../services/review/review.service';
import {Manager} from '../model/manager';
import Swal from 'sweetalert2';
import {MaponePage} from '../mapone/mapone.page';
import {Arret} from "../model/arret";
import {TypeTransport} from "../model/typeTransport";


@Component({
  selector: 'app-detailline',
  templateUrl: './detailline.page.html',
  styleUrls: ['./detailline.page.scss'],
})
export class DetaillinePage implements OnInit {
  public id: number;
  public lines: Line;
  constructor(private router: Router, private callNumber: CallNumber, private otherService: OtherService,
              private modalController: ModalController, private reviewService: ReviewService,
  ) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      idline: number;
    };
    if (state == null || state.idline === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      console.log(state);
      this.id = state.idline;
      this.lines= new Line();
      this.lines.arrive= new Arret();
      this.lines.depart= new Arret();
      this.lines.typeTransport= new TypeTransport();
      this.lines.arret= [new Arret()];

    }
  }

  ngOnInit() {

    this.detailline();
  }

  detailline(){
    this.otherService.linebyid(this.id).subscribe(text => {
      console.log(text);
      this.lines = text;
    });
  }


  appel(numero) {
    if (numero === 'none'){
      Swal.fire({
        icon: 'error', title: 'Le numéro est indisponible', showConfirmButton: false, timer: 3000,
      });
    }
    else {

      this.callNumber.callNumber(numero, true)
        .then(res =>{ console.log('Launched dialer!', res);
          this.otherService.callclick(this.id,this.lines).subscribe(call => {
            console.log(call);
            this.detailline();
          });})
        .catch(err => console.log('Error launching dialer', err));
    }
  }

  async emplacement(id) {
    const signalerModal = await this.modalController.create({
      component: MaponePage,
      cssClass: 'my-custom-class',
      swipeToClose: true, componentProps: {
        id
      }

    });
    return await signalerModal.present();

  }


  statutline(statut) {
    if (statut == 0){
      Swal.fire({
        icon: 'error', title: 'La ligne est indisponible', showConfirmButton: false, timer: 3000,
      });
    }
    else if (statut == 3){
      Swal.fire({
        icon: 'warning', title: 'Aucune information sur cette ligne', showConfirmButton: false, timer: 3000,
      });
    }
    else if (statut == 2){
      Swal.fire({
        icon: 'error', title: 'Cette ligne est en maintenance', showConfirmButton: false, timer: 3000,
      });
    }
    else {
      Swal.fire({
        icon: 'success', title: 'Cette ligne est disponible', showConfirmButton: false, timer: 3000,
      });}
  }
}
