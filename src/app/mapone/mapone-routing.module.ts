import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MaponePage } from './mapone.page';

const routes: Routes = [
  {
    path: '',
    component: MaponePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MaponePageRoutingModule {}
