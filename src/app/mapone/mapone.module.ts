import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MaponePageRoutingModule } from './mapone-routing.module';

import { MaponePage } from './mapone.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MaponePageRoutingModule
  ],
  declarations: [MaponePage]
})
export class MaponePageModule {}
