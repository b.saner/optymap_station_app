import { Component,Input, OnInit } from '@angular/core';
import {Map, tileLayer, marker, icon, circle, polygon, geoJSON, Marker} from 'leaflet';
import 'leaflet-routing-machine';
import * as L1 from 'leaflet.markercluster';
import * as L from 'leaflet';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import {ModalController} from '@ionic/angular';
import {NavigationExtras, Router} from '@angular/router';
import {StationService} from '../services/station/station.service';

const meicon = L.icon({
  iconUrl: '../assets/man.png',
  iconSize: [25, 41],
  iconAnchor: [10, 41],
  popupAnchor: [2, -45],});
const meicon2 = L.icon({
  iconUrl: '../assets/icon-map.png',
  iconSize: [25, 41],
  iconAnchor: [10, 41],
  popupAnchor: [2, -45],});
@Component({
  selector: 'app-mapone',
  templateUrl: './mapone.page.html',
  styleUrls: ['./mapone.page.scss'],
})
export class MaponePage implements OnInit {
  map: Map;
  newMarker: any;
  newMarker2: any;
  public locations: any;
  @Input() id: string;
  public latlong: any;
  public wlatitude: any;
  public wlongitude: any;
  constructor(private geolocation: Geolocation, public modalController: ModalController,
              private mapservice: StationService, private router: Router) { }

  ngOnInit() {
  }
  ionViewDidEnter() {
    this.map = new Map('mapIditi');
    tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',

    ).addTo(this.map);
    // }
    this.locatePosition();
    // this.ittineraire();
  }
  locatePosition() {
    this.map.locate({ watch: false, setView: false, maxZoom: 12, enableHighAccuracy : true}).on('locationfound', (e: any) => {
      console.log('location', e);
      this.wlatitude = e.latitude;
      this.wlongitude = e.longitude;
      // Si la localisation est disponible on integrera notre position
      // this.newMarker = marker([e.latitude, e.longitude], {
      //   draggable: false,
      //
      // }).addTo(this.map);
      // this.newMarker.bindPopup('<p>Ma position</p>');
      this.newMarker2 = marker([e.latitude, e.longitude],{
        icon: meicon,
      }).addTo(this.map);
      this.mapservice.stationbyid(this.id).subscribe(positions => {
        this.locations = positions;
        console.log(positions);
        L.marker([positions.latitude, positions.longitude],{        icon: meicon2,
        })
          .addTo(this.map);

        // tslint:disable-next-line:only-arrow-functions
        //   .on('click', event =>  {
        // this.router.navigate(['/signalisationdetail'], navigationExtras);

        //   });
        L.Routing.control({
          router: L.Routing.osrmv1({
            serviceUrl: `https://router.project-osrm.org/route/v1/`,
            language: 'fr',
            profile: 'car'
          }),
          // showAlternatives: true,
          fitSelectedRoutes: 'smart',
          routeWhileDragging: false,
          show: false,
          waypoints: [
            L.latLng(e.latitude, e.longitude),
            // eslint-disable-next-line radix
            L.latLng(this.locations.latitude, this.locations.longitude)
          ],
        }).addTo(this.map);
        // Lr.Routing.control({
        //   waypoints: [
        //     L.latLng(positions.latitude, positions.longitude),
        //     L.latLng(positions.latitude+1, positions.longitude+1)
        //     // L.latLng(this.latlong.latlng, this.latlong.lng)
        //   ], router: new Lr.Routing.osrmv1({
        //     language: 'fr',
        //     profile: 'car'
        //   }),
        //   geocoder: Lr.Control.Geocoder.nominatim({})

        // }).addTo(this.map);
      });

    });
  }
  locatePositionpres() {
    console.log('test');
    this.map.setView(this.latlong, 16);

    //   this.map.locate().on('locationfound', (e: any) => {
    //   console.log('location', e);
    //   // Si la localisation est disponible on integrera notre position
    //   // this.newMarker = marker([e.latitude, e.longitude], {
    //   //   draggable: false,
    //   //   icon: meicon,
    //   //
    //   // }).addTo(this.map);
    //   // this.newMarker.bindPopup('<p>Ma position</p>');
    // });
  }


  // ittineraire(){
  //
  //   this.mapservice.stationbyid(this.id).subscribe(positions => {
  //     this.locations = positions;
  //     console.log(positions);
  //     console.log(positions.length);
  //     L.marker([positions.latitude, positions.longitude])
  //       .addTo(this.map);
  //
  //       // tslint:disable-next-line:only-arrow-functions
  //    //   .on('click', event =>  {
  //         // this.router.navigate(['/signalisationdetail'], navigationExtras);
  //
  //    //   });
  //     L.Routing.control({
  //       router: L.Routing.osrmv1({
  //         serviceUrl: `http://router.project-osrm.org/route/v1/`,
  //         language: 'fr',
  //         profile: 'car'
  //       }),
  //       showAlternatives: true,
  //       fitSelectedRoutes: 'smart',
  //       routeWhileDragging: false,
  //       show: false,
  //       waypoints: [
  //         L.latLng(this.wlatitude, this.wlongitude),
  //         // eslint-disable-next-line radix
  //           L.latLng(this.locations.latitude, this.locations.longitude)
  //       ]
  //     }).addTo(this.map);
  //     // Lr.Routing.control({
  //     //   waypoints: [
  //     //     L.latLng(positions.latitude, positions.longitude),
  //     //     L.latLng(positions.latitude+1, positions.longitude+1)
  //     //     // L.latLng(this.latlong.latlng, this.latlong.lng)
  //     //   ], router: new Lr.Routing.osrmv1({
  //     //     language: 'fr',
  //     //     profile: 'car'
  //     //   }),
  //     //   geocoder: Lr.Control.Geocoder.nominatim({})
  //
  //     // }).addTo(this.map);
  //   });
  // }

  ionViewDidLeave() {
    this.map.remove();
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true
    });
  }
}
