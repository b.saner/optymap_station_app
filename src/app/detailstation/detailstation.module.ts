import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailstationPageRoutingModule } from './detailstation-routing.module';

import { DetailstationPage } from './detailstation.page';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailstationPageRoutingModule,
    TranslateModule
  ],
  declarations: [DetailstationPage]
})
export class DetailstationPageModule {}
