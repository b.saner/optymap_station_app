import { Component, OnInit } from '@angular/core';
import {NavigationExtras, Router} from '@angular/router';
import {CallNumber} from '@ionic-native/call-number/ngx';
import {StationService} from '../services/station/station.service';
import {Station} from '../model/station';
import Swal from 'sweetalert2';
import {Entretien} from '../model/entretien';
import {Culte} from '../model/culte';
import {Gaz} from '../model/gaz';
import {Gazoil} from '../model/gazoil';
import {Airederepos} from '../model/airederepos';
import {Superette} from '../model/superette';
import {Gab} from '../model/gab';
import {Electrique} from '../model/electrique';
import {Essence} from '../model/essence';
import {Gpl} from '../model/gpl';
import {Toilette} from '../model/toilette';
import {ModalController, PopoverController} from '@ionic/angular';
import {MaponePage} from "../mapone/mapone.page";
import {DetailpopupPage} from "../detailpopup/detailpopup.page";
import {PopupcallPage} from "../popupcall/popupcall.page";

@Component({
  selector: 'app-detailstation',
  templateUrl: './detailstation.page.html',
  styleUrls: ['./detailstation.page.scss'],
})
export class DetailstationPage implements OnInit {
  public id: number;
  public stations: Station;
  constructor(private router: Router, private callNumber: CallNumber, private stationservice: StationService,
              private modalController: ModalController, private popoverController: PopoverController) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      idstation: number;
    };
    if (state == null || state.idstation === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      this.id = state.idstation;
      console.log(this.id);
      this.stations= new Station();
      this.stations.entretien = new Entretien();
      this.stations.essence = new Essence();
      this.stations.culte = new Culte();
      this.stations.gaz = new Gaz();
      this.stations.gazoil = new Gazoil();
      this.stations.airederepos = new Airederepos();
      this.stations.superette = new Superette();
      this.stations.gab = new Gab();
      this.stations.electrique = new Electrique();
      this.stations.gpl = new Gpl();
      this.stations.toilette = new Toilette();
    }
  }

  ngOnInit() {

    this.detailstation();
  }

  detailstation(){
    this.stationservice.stationbyid(this.id).subscribe(text => {
      console.log(text);
      this.stations = text;
    });
  }
  gotoessence(statut) {
    console.log(statut);
    if (statut == 0){
      Swal.fire({
        icon: 'error', title: 'Le service est indisponible', showConfirmButton: false, timer: 3000,
      });
    }
     else if (statut == 1){
      const idessence = this.stations.essence.id;
      const navigationExtras: NavigationExtras = {
        state: {idessence }
      };
      this.router.navigate(['/essence'], navigationExtras);
      }
    else if (statut == 2){
      Swal.fire({
        icon: 'warning', title: 'Ce service est en panne ou en renovation', showConfirmButton: false, timer: 3000,
      });
    }
    else {
      Swal.fire({
        icon: 'error', title:'Aucune information sur ce service', showConfirmButton: false, timer: 3000,
      });
  }
  }

  gotogpl(statut) {
    console.log(statut);
    if (statut == 0){
      Swal.fire({
        icon: 'error', title: 'Le service est indisponible', showConfirmButton: false, timer: 3000,
      });
    }
     else if (statut == 1){
      const idgpl = this.stations.gpl.id;
      const navigationExtras: NavigationExtras = {
        state: {idgpl }
      };
      this.router.navigate(['/gpl'], navigationExtras);
      }
    else if (statut == 2){
      Swal.fire({
        icon: 'warning', title: 'Ce service est en panne ou en renovation', showConfirmButton: false, timer: 3000,
      });
    }
    else {
      Swal.fire({
        icon: 'error', title:'Aucune information sur ce service', showConfirmButton: false, timer: 3000,
      });
  }
  }
  gotoculte(statut) {
    if (statut == 0){
      Swal.fire({
        icon: 'error', title: 'Le service est indisponible', showConfirmButton: false, timer: 3000,
      });
    }
    else if (statut == 2){
      Swal.fire({
        icon: 'warning', title: 'Ce service est en panne', showConfirmButton: false, timer: 3000,
      });
    }
     else if (statut == 3){
        Swal.fire({
          icon: 'error', title:'Aucune information sur ce service', showConfirmButton: false, timer: 3000,
        });
      }
    else {
      const idculte = this.stations.culte.id;
      const navigationExtras: NavigationExtras = {
        state: {idculte }
      };
      this.router.navigate(['/culte'], navigationExtras);
    }
  }
  gotoentretien(statut) {
      if (statut == 0){
        Swal.fire({
          icon: 'error', title: 'Le service est indisponible', showConfirmButton: false, timer: 3000,
        });
      }
       else if (statut == 3){
        Swal.fire({
          icon: 'error', title: 'Aucune information sur ce service', showConfirmButton: false, timer: 3000,
        });
      }
        else if (statut == 2){
        Swal.fire({
          icon: 'warning', title: 'Ce service est en panne', showConfirmButton: false, timer: 3000,
        });
      }
      else {
        const identretien = this.stations.entretien.id;
        const navigationExtras: NavigationExtras = {
          state: {identretien }
        };
        this.router.navigate(['/entretien'],navigationExtras);
      }
  }
  gotogaz(statut) {
      if (statut == 0){
        Swal.fire({
          icon: 'error', title: 'Le service est indisponible', showConfirmButton: false, timer: 3000,
        });
      }
       else if (statut == 3){
        Swal.fire({
          icon: 'error', title: 'Aucune information sur ce service', showConfirmButton: false, timer: 3000,
        });
      }
        else if (statut == 2){
        Swal.fire({
          icon: 'warning', title: 'Ce service est en panne', showConfirmButton: false, timer: 3000,
        });
      }
      else {
        const idgaz= this.stations.gaz.id;
        const navigationExtras: NavigationExtras = {
          state: {idgaz }
        };
        this.router.navigate(['/gaz'],navigationExtras);
      }
  }
  gotoelectrique(statut) {
      if (statut == 0){
        Swal.fire({
          icon: 'error', title: 'Le service est indisponible', showConfirmButton: false, timer: 3000,
        });
      }
       else if (statut == 3){
        Swal.fire({
          icon: 'error', title: 'Aucune information sur ce service', showConfirmButton: false, timer: 3000,
        });
      }
        else if (statut == 2){
        Swal.fire({
          icon: 'warning', title: 'Ce service est en panne', showConfirmButton: false, timer: 3000,
        });
      }
      else {
        const idelectrique= this.stations.electrique.id;
        const navigationExtras: NavigationExtras = {
          state: {idelectrique }
        };
        this.router.navigate(['/electrique'],navigationExtras);
      }
  }
  gotogab(statut) {
      if (statut == 0){
        Swal.fire({
          icon: 'error', title: 'Le service est indisponible', showConfirmButton: false, timer: 3000,
        });
      }
       else if (statut == 3){
        Swal.fire({
          icon: 'error', title: 'Aucune information sur ce service', showConfirmButton: false, timer: 3000,
        });
      }
        else if (statut == 2){
        Swal.fire({
          icon: 'warning', title: 'Ce service est en panne', showConfirmButton: false, timer: 3000,
        });
      }
      else {
        const idgab = this.stations.gab.id;
        const navigationExtras: NavigationExtras = {
          state: {idgab}
        };
        this.router.navigate(['/gab'],navigationExtras);
      }
  }
  gotorepos(statut) {
      if (statut == 0){
        Swal.fire({
          icon: 'error', title: 'Le service est indisponible', showConfirmButton: false, timer: 3000,
        });
      }
       else if (statut == 3){
        Swal.fire({
          icon: 'error', title: 'Aucune information sur ce service', showConfirmButton: false, timer: 3000,
        });
      }
        else if (statut == 2){
        Swal.fire({
          icon: 'warning', title: 'Ce service est en panne', showConfirmButton: false, timer: 3000,
        });
      }
      else {
        const idaire = this.stations.airederepos.id;
        const navigationExtras: NavigationExtras = {
          state: {idaire}
        };
        this.router.navigate(['/repos'],navigationExtras);
      }
  }
  gotosuperette(statut) {
      if (statut == 0){
        Swal.fire({
          icon: 'error', title: 'Le service est indisponible', showConfirmButton: false, timer: 3000,
        });
      }
       else if (statut == 3){
        Swal.fire({
          icon: 'error', title: 'Aucune information sur ce service', showConfirmButton: false, timer: 3000,
        });
      }
        else if (statut == 2){
        Swal.fire({
          icon: 'warning', title: 'Ce service est en panne', showConfirmButton: false, timer: 3000,
        });
      }
      else {
        const idsuperette = this.stations.superette.id;
        const navigationExtras: NavigationExtras = {
          state: {idsuperette}
        };
        this.router.navigate(['/superette'],navigationExtras);
      }
  }

  gotogazoil(statut) {
      if (statut == 0){
        Swal.fire({
          icon: 'error', title: 'Le service est indisponible', showConfirmButton: false, timer: 3000,
        });
      }
       else if (statut == 3){
        Swal.fire({
          icon: 'error', title: 'Aucune information sur ce service', showConfirmButton: false, timer: 3000,
        });
      }
        else if (statut == 2){
        Swal.fire({
          icon: 'warning', title: 'Ce service est en panne', showConfirmButton: false, timer: 3000,
        });
      }

      else {
        const idgazoil = this.stations.gazoil.id;
        const navigationExtras: NavigationExtras = {
          state: {idgazoil}
        };
        this.router.navigate(['/gazoil'],navigationExtras);
      }
  }
  gototoilette(statut) {
      if (statut == 0){
        Swal.fire({
          icon: 'error', title: 'Le service est indisponible', showConfirmButton: false, timer: 3000,
        });
      }
       else if (statut == 3){
        Swal.fire({
          icon: 'error', title: 'Aucune information sur ce service', showConfirmButton: false, timer: 3000,
        });
      }
        else if (statut == 2){
        Swal.fire({
          icon: 'warning', title: 'Ce service est en panne', showConfirmButton: false, timer: 3000,
        });
      }

      else {
        const idgazoil = this.stations.gazoil.id;
        const navigationExtras: NavigationExtras = {
          state: {idgazoil}
        };
        this.router.navigate(['/toilette'],navigationExtras);
      }
  }
  appel(numero) {
    if (numero === 'none'){
      Swal.fire({
        icon: 'error', title: 'Le numéro est indisponible', showConfirmButton: false, timer: 3000,
      });
    }
    else {
    this.callNumber.callNumber(numero, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }
}

  async emplacement(id) {
    const signalerModal = await  this.modalController.create({component: MaponePage,
      cssClass: 'my-custom-class',
      swipeToClose: true, componentProps: {
        id
      }

    });
    return await signalerModal.present();

  }
  async infocall(tel: string) {
    if (tel == null || tel =='none' ||tel ==undefined){
      Swal.fire({
        icon: 'error', title: 'Le numéro est indisponible', showConfirmButton: false, timer: 2500,
      });
    }
    else{
    const callModal = await this.modalController.create({
      component: PopupcallPage,
      cssClass: 'cmodal',
      swipeToClose: true, componentProps: {
        tel
      }
    });
    return await callModal.present();
    }
  }

  // async presentPopover(ev: any) {
  //   const popover = await this.popoverController.create({
  //     component: PopupcallPage,
  //     cssClass: 'my-custom-class',
  //     event: ev,
  //     translucent: true
  //   });
  //   await popover.present();
  //
  //   const { role } = await popover.onDidDismiss();
  //   console.log('onDidDismiss resolved with role', role);
  // }
}
