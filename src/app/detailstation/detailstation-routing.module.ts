import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailstationPage } from './detailstation.page';

const routes: Routes = [
  {
    path: '',
    component: DetailstationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailstationPageRoutingModule {}
