import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuperettePageRoutingModule } from './superette-routing.module';

import { SuperettePage } from './superette.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuperettePageRoutingModule
  ],
  declarations: [SuperettePage]
})
export class SuperettePageModule {}
