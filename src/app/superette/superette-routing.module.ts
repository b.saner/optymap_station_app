import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuperettePage } from './superette.page';

const routes: Routes = [
  {
    path: '',
    component: SuperettePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuperettePageRoutingModule {}
