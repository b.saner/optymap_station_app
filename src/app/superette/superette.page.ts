import { Component, OnInit } from '@angular/core';
import {NavigationExtras, Router} from '@angular/router';
import {DetailstationService} from '../services/detailstation/detailstation.service';
import {Superette} from '../model/superette';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-superette',
  templateUrl: './superette.page.html',
  styleUrls: ['./superette.page.scss'],
})
export class SuperettePage implements OnInit {

  public id: number;
  public superette: Superette;

  constructor(private router: Router, private stationservice: DetailstationService) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      idsuperette: number;
    };
    if (state == null || state.idsuperette === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      this.id = state.idsuperette;
      console.log(this.id);
    }
  }

  ngOnInit() {
    this.detailsuperette();
  }

  detailsuperette(){
    this.stationservice.superettebyid(this.id).subscribe(text => {
      console.log(text);
      this.superette = text;
    });
  }

  gotomoney(money, typeservice) {
    if (money == null || money == 0){
      Swal.fire({
        icon: 'error', title: 'Le service est indisponible', showConfirmButton: false, timer: 3000,
      });
    }
    else if ( money == 2) {
      Swal.fire({
        icon: 'error', title: 'Le service est maintenance', showConfirmButton: false, timer: 3000,
      });
    }   else if ( money == 3){
      Swal.fire({
        icon: 'error', title: 'Pas d\'information sur le service ', showConfirmButton: false, timer: 3000,
      });
    }
    else {
      const idemoney = this.superette.id;
      const type = typeservice;
      const navigationExtras: NavigationExtras = {
        state: {idemoney,type}
      };
      this.router.navigate(['/e-money'],navigationExtras);
  }
}
}
