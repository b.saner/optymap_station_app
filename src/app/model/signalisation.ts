import {Manager} from './manager';
import {Utilisateur} from './utilisateur';
import {Statut} from './statut';

export class Signalisation {
  motif: string;
  user: Utilisateur;
  statut: Statut;
  responsable: string;
  latitude: string;
  longitude: string;
  categorie: string;
  souscategorie: string;
  commentaire: string;
  photo: string;
  id: string;
}
