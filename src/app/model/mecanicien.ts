export class Mecanicien {
  id: number;
    latitude: string;
    longitude: string;
    nom: string;
    prenom: string;
    mail: string;
    tel: string;
    vdiagnostic: number;
    remorquage: number;
    carosserie: number;
    depannage: number;
    electrique: number;
    mecanique: number;
    certification: number;
    callclick : number;
    departement: string;
    localite: string;
    region: string;
    type: string;
}
