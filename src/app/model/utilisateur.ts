export class Utilisateur {
    id: number;
    telephone: string;
    type: number;
    huissierid: string;
    password: string;
    nom: string;
    email: string;
    prenom: string;
    adresse: string;
    sexe: string;
    chambre: number;
    departement: string;
    region: string;
}
