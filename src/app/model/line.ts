import {Arret} from './arret';
import {TypeTransport} from './typeTransport';

export class Line {
  id: number;
  nom: string;
  region: string;
  arret: [Arret];
  arrive:	Arret;
  date1week: string;
  date1work: string;
  date2week: string;
  date2work: string;
  depart:	Arret;
  statut: number;
  typeTransport: TypeTransport;
}
