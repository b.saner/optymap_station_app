export class Hopital {
    id: number;
    latitude: string;
    longitude: string;
    nom: string;
    tel: string;
    certification: number;
    callclick: number;
    statut: boolean;
    departement: string;
    localite: string;
    region: string;
    type: string;
    ninea: string;
    mail: string;
  cardiologie: number;
  pharmacie: number;
  traumatologie: number;
  sgb: number;
  maternite: number;
  pediatrie: number;
  urologie: number;
  pneumologie: number;
  neurologie: number;
  radiologie: number;
  dermatologie: number;
  medecinegenerale: number;
  ophtalmologie: number;
  chirugie: number;
  immunologie: number;
  odontologie: number;
  endocrinologie: number;
  hematologie: number;
  gastroenterologie: number;
  su: number;



}
