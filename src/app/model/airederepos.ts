export class Airederepos {
    id: number;
    photos: [string];
    statut: number;
  airedejeu: number;
  airedepicnic: number;
  eau: number;
  espaceenfant: number;
  gonflage: number;
  handicape: number;
  ppc: number;
  ppl: number;
  pvl: number;

}
