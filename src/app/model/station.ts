import { Airederepos } from './airederepos';
import { Culte } from './culte';
import { Electrique } from './Electrique';
import { Entretien } from './entretien';
import { Gab } from './gab';
import { Gazoil } from './gazoil';
import { Superette } from './superette';
import {Essence} from './essence';
import {Gaz} from './gaz';
import {Gpl} from './gpl';
import {Toilette} from './toilette';
import {Manager} from './manager';

export class Station {
    id: number;
    tel: string;
    airederepos: Airederepos;
    culte: Culte;
    electrique: Electrique;
    entretien: Entretien;
    essence: Essence;
    gab: Gab;
    gazoil: Gazoil;
    gaz: Gaz;
    latitude: string;
    logo: string;
    longitude: string;
    nom: string;
    toilette: Toilette;
    superette: Superette;
    gpl: Gpl;
    manager: Manager;
  departement: string;
  localite: string;
  region: string;
}
