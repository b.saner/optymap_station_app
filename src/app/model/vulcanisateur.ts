
export class Vulcanisateur {
    id: number;
    latitude: string;
    longitude: string;
    nom: string;
    prenom: string;
    tel: string;
    certification: number;
    callclick: number;
    statut: boolean;
    departement: string;
    localite: string;
    region: string;
    type: string;
    ninea: string;
  disponibilite: string;
  equilibrage: string;
  vente: string;
  depannage: number;
  reparation: number;
  mail: string;

}
