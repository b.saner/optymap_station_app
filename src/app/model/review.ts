import {Utilisateur} from './utilisateur';
import {Mecanicien} from './mecanicien';

export class Review {
    id: number;
    commentaire: string;
    nom: string;
    rating: number;
    mecanicien: Mecanicien;
    reviewer: Utilisateur;

}
