
export class Gendarmerie {
    id: number;
    latitude: string;
    longitude: string;
    nom: string;
    tel: string;
    certification: number;
    callclick: number;
    statut: boolean;
    departement: string;
    localite: string;
    region: string;
    type: string;
    mail: string;
}
