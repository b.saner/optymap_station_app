import {Typeservice} from './typeservice';
import {Utilisateur} from './utilisateur';

export class Callservice {
    id: number;
    datecall: Date;
    serviceid: number;
    nom: string;
    typeservice: Typeservice;
    user: Utilisateur;
}
