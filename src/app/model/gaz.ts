export class Gaz {
  id: number;
  client: string;
  prix: number;
  statut: number;
  nom27: string;
  prix27: number;
  statut27: number;
  nom6: string;
  prix6: number;
  statut6: number;
  nom9: string;
  prix9: number;
  statut9: number;
  nom125: string;
  prix125: number;
  statut125: number;
  nom32: string;
  prix32: number;
  statut32: number;
  nom38: string;
  prix38: number;
  statut38: number;
}
