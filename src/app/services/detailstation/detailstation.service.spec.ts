import { TestBed } from '@angular/core/testing';

import { DetailstationService } from './detailstation.service';

describe('DetailstationService', () => {
  let service: DetailstationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DetailstationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
