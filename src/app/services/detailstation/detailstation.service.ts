import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {
  airederepos,
  apiDev,
  culte,
  electrique,
  entretien,
  essence,
  gab, gaz, gazoil, gpl,
  station, superette, toilette
} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DetailstationService {

  constructor(private http: HttpClient) { }

  airedereposbyid(id): Observable<any> {
    return this.http.get(`${apiDev}${airederepos}/` + id);
  }
  cultebyid(id): Observable<any> {
    return this.http.get(`${apiDev}${culte}/` + id);
  }
  electriquebyid(id): Observable<any> {
    return this.http.get(`${apiDev}${electrique}/` + id);
  }
  entretienbyid(id): Observable<any> {
    return this.http.get(`${apiDev}${entretien}/` + id);
  }
  essencebyid(id): Observable<any> {
    return this.http.get(`${apiDev}${essence}/` + id);
  }
  gabbyid(id): Observable<any> {
    return this.http.get(`${apiDev}${gab}/` + id);
  }
  gazbyid(id): Observable<any> {
    return this.http.get(`${apiDev}${gaz}/` + id);
  }
  gazoilbyid(id): Observable<any> {
    return this.http.get(`${apiDev}${gazoil}/` + id);
  }
  gplbyid(id): Observable<any> {
    return this.http.get(`${apiDev}${gpl}/` + id);
  }
  superettebyid(id): Observable<any> {
    return this.http.get(`${apiDev}${superette}/` + id);
  }

  toilettebyid(id: number): Observable<any> {
      return this.http.get(`${apiDev}${toilette}/` + id);

  }
}
