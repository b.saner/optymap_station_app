import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {apiDev, callservice} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CallServices {

  constructor(private http: HttpClient) { }

  callservice(data): Observable<any> {
    return this.http.post(`${apiDev}${callservice}/`,data);
  }
}
