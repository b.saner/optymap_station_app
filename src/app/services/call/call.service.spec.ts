import { TestBed } from '@angular/core/testing';

import { CallServices } from './call.service';

describe('CallService', () => {
  let service: CallServices;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CallServices);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
