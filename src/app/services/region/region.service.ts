import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {apiDev, departement, departementbyregion, region} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegionService {

  constructor(private http: HttpClient){ }

  allregion(): Observable<any> {
    return this.http.get(`${apiDev}${region}`);
  }
  alldepartment(): Observable<any> {
    return this.http.get(`${apiDev}${departement}`);
  }

  departmentbyregion(id): Observable<any> {
    return this.http.get(`${apiDev}${departementbyregion}` + id);
  }
  departmentbyid(id): Observable<any> {
    return this.http.get(`${apiDev}${departement}` + id);
  }
}
