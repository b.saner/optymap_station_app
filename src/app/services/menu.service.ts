import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  private logSubject = new Subject<any>();
  private styleSubject = new Subject<any>();

  constructor() { }

  iAmlog(data: any) {
    this.logSubject.next(data);
  }

  getObservable(): Subject<any> {
    return this.logSubject;
  }

  mystyleset(data: any) {
    this.styleSubject.next(data);
  }
  mystyleget(): Subject<any>{
    return this.styleSubject;
  }
}
