import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {apiDev, station} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StationService {

  constructor(private http: HttpClient) { }
  allstation(): Observable<any> {
    return this.http.get(`${apiDev}${station}`);
  }
  stationbyid(id): Observable<any> {
    return this.http.get(`${apiDev}${station}/` + id);
  }
  searchstation(term): Observable<any> {
    return this.http.get(`${apiDev}${station}/search/` + term);
  }
  addstation(stations): Observable<any> {
    return this.http.post(`${apiDev}${station}/`,stations);
  }
  putstation(id,stations): Observable<any> {
    return this.http.put(`${apiDev}${station}/`+id,stations);
  }
}
