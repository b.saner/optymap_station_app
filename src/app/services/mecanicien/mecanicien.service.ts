import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {apiDev, mecanicien, mecaniciencall} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MecanicienService {

  constructor(private http: HttpClient) { }
  allmecanicien(): Observable<any> {
    return this.http.get(`${apiDev}${mecanicien}`);
  }
  mecanicienbyid(id): Observable<any> {
    return this.http.get(`${apiDev}${mecanicien}/` + id);
  }
  searchmecanicien(term): Observable<any> {
    return this.http.get(`${apiDev}${mecanicien}/search/` + term);
  }
  addmecanicien(mecaniciens): Observable<any> {
    return this.http.post(`${apiDev}${mecanicien}/`,mecaniciens);
  }
  putmecanicien(id,mecaniciens): Observable<any> {
    return this.http.put(`${apiDev}${mecanicien}/`+id,mecaniciens);
  }

  callmecanicien(id,data): Observable<any> {
      return this.http.post(`${apiDev}${mecaniciencall}/`+id,data);
  }
}
