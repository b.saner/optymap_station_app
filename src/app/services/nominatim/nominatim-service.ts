import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {NominatimResponse} from '../../model/nomresponse';
import {map} from 'rxjs/operators';

 const BASE_NOMINATIM_URL = 'nominatim.openstreetmap.org';
 const DEFAULT_VIEW_BOX = 'viewbox=-25.0000%2C70.0000%2C50.0000%2C40.0000';

@Injectable({
  providedIn: 'root'
})
export class NominatimService {

  constructor(private http: HttpClient) {
  }

  addressLook(req?: any): Observable<NominatimResponse[]> {
    const url = `https://`+BASE_NOMINATIM_URL+`/search?format=json&q=`+req;
    return this.http
      .get(url).pipe(
        map((data: any[]) => data.map((item: any) => new NominatimResponse(
          item.lat,
          item.lon,
          item.display_name
          ))
        )
      );
  }

}
