import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {apiDev, rating, ratingBymecanicien, ratingByuser, reviewBymecanicien} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  constructor(private http: HttpClient) { }


  rating(review): Observable<any> {
    return this.http.post(`${apiDev}${rating}`, review);
  }
  myratingbymecanicien(id): Observable<any> {
    return this.http.get(`${apiDev}${rating}${ratingBymecanicien}${id}`);
  }
  myreviewbymecanicien(id): Observable<any> {
    return this.http.get(`${apiDev}${rating}${reviewBymecanicien}${id}`);
  }
  myreviewmecanicienbyuser(id,id2): Observable<any> {
    return this.http.get(`${apiDev}${rating}${reviewBymecanicien}${id}/${id2}`);
  }
  myreviewbyuser(id): Observable<any> {
    return this.http.get(`${apiDev}${rating}${ratingByuser}${id}`);
  }
}
