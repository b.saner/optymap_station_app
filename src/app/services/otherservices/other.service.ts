import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {
  apiDev,
  signalisation,
  pharmacie,
  pharmaciecall,
  line,
  arret,
  orange,
  vulcanisateur, hopital, gendarmerie
} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OtherService {

  constructor(private http: HttpClient) { }

  //signalisations services

  allsignalisation(): Observable<any> {
    return this.http.get(`${apiDev}${signalisation}`);
  }
  signalisationbyid(id): Observable<any> {
    return this.http.get(`${apiDev}${signalisation}/` + id);
  }
  searchsignalisation(term): Observable<any> {
    return this.http.get(`${apiDev}${signalisation}/search/` + term);
  }
  addsignalisation(signalisations): Observable<any> {
    return this.http.post(`${apiDev}${signalisation}`,signalisations);
  }
  putsignalisation(id,signalisations): Observable<any> {
    return this.http.put(`${apiDev}${signalisation}/`+id,signalisations);
  }


  //pharmacies services

  allpharmacie(): Observable<any> {
    return this.http.get(`${apiDev}${pharmacie}`);
  }
  pharmaciebyid(id): Observable<any> {
    return this.http.get(`${apiDev}${pharmacie}/` + id);
  }
  searchpharmacie(term): Observable<any> {
    return this.http.get(`${apiDev}${pharmacie}/search/` + term);
  }
  addpharmacie(pharmacies): Observable<any> {
    return this.http.post(`${apiDev}${pharmacie}`,pharmacies);
  }
  putpharmacie(id,pharmacies): Observable<any> {
    return this.http.put(`${apiDev}${pharmacie}/`+id,pharmacies);
  }

  callclick(id,pharmacies): Observable<any> {
    return this.http.put(`${apiDev}${pharmaciecall}/`+id,pharmacies);
  }
  // les services des  arrets

  addarret(arrets): Observable<any> {
    return this.http.post(`${apiDev}${arret}`,arrets);
  }
  allarret(): Observable<any> {
    return this.http.get(`${apiDev}${arret}`);
  }
  arretbyid(id): Observable<any> {
    return this.http.get(`${apiDev}${arret}/` + id);
  }
  linebyarretid(id): Observable<any> {
    return this.http.get(`${apiDev}${line}${arret}/` + id);
  }
  searcharret(term): Observable<any> {
    return this.http.get(`${apiDev}${arret}/search/` + term);
  }
  putarret(id,arrets): Observable<any> {
    return this.http.put(`${apiDev}${arret}/`+id,arrets);
  }


  // les services des  vulcanisateurs

  addvulcanisateur(vulcanisateurs): Observable<any> {
    return this.http.post(`${apiDev}${vulcanisateur}`,vulcanisateurs);
  }
  allvulcanisateur(): Observable<any> {
    return this.http.get(`${apiDev}${vulcanisateur}`);
  }
  vulcanisateurbyid(id): Observable<any> {
    return this.http.get(`${apiDev}${vulcanisateur}/` + id);
  }

  searchvulcanisateur(term): Observable<any> {
    return this.http.get(`${apiDev}${vulcanisateur}/search/` + term);
  }
  putvulcanisateur(id,vulcanisateurs): Observable<any> {
    return this.http.put(`${apiDev}${vulcanisateur}/`+id,vulcanisateurs);
  }

  // les services des  hopitals

  addhopital(hopitals): Observable<any> {
    return this.http.post(`${apiDev}${hopital}`,hopitals);
  }
  allhopital(): Observable<any> {
    return this.http.get(`${apiDev}${hopital}`);
  }
  hopitalbyid(id): Observable<any> {
    return this.http.get(`${apiDev}${hopital}/` + id);
  }

  searchhopital(term): Observable<any> {
    return this.http.get(`${apiDev}${hopital}/search/` + term);
  }
  puthopital(id,hopitals): Observable<any> {
    return this.http.put(`${apiDev}${hopital}/`+id,hopitals);
  }

  // les services des  line

  addline(lines): Observable<any> {
    return this.http.post(`${apiDev}${line}`,lines);
  }
  allline(): Observable<any> {
    return this.http.get(`${apiDev}${line}`);
  }
  linebyid(id): Observable<any> {
    return this.http.get(`${apiDev}${line}/` + id);
  }
  searchline(term): Observable<any> {
    return this.http.get(`${apiDev}${line}/search/` + term);
  }
  putline(id,lines): Observable<any> {
    return this.http.put(`${apiDev}${line}/`+id,lines);
  }

  //orange services

  allorange(): Observable<any> {
    return this.http.get(`${apiDev}${orange}`);
  }
  orangebyid(id): Observable<any> {
    return this.http.get(`${apiDev}${orange}/` + id);
  }
  searchorange(term): Observable<any> {
    return this.http.get(`${apiDev}${orange}/search/` + term);
  }
  addorange(oranges): Observable<any> {
    return this.http.post(`${apiDev}${orange}`,oranges);
  }
  putorange(id,oranges): Observable<any> {
    return this.http.put(`${apiDev}${orange}/`+id,oranges);
  }

  orangecallclick(id,oranges): Observable<any> {
    return this.http.put(`${apiDev}${orange}/`+id,oranges);
  }



// gendarmerie service

  allgendarmerie(): Observable<any> {
    return this.http.get(`${apiDev}${gendarmerie}`);
  }

  searchgendarmerie(term): Observable<any> {
    return this.http.get(`${apiDev}${gendarmerie}/search/` + term);
  }
  addgendarmeries(gendarmeries): Observable<any> {
    return this.http.post(`${apiDev}${gendarmerie}`,gendarmeries);
  }
  putgendarmerie(id,gendarmeries): Observable<any> {
    return this.http.put(`${apiDev}${gendarmerie}/`+id,gendarmeries);
  }

  gendarmeriecallclick(id,gendarmeries): Observable<any> {
    return this.http.put(`${apiDev}${gendarmerie}/`+id,gendarmeries);
  }

  gendarmeriebyid(id): Observable<any> {
    return this.http.get(`${apiDev}${gendarmerie}/` + id);
  }
}
