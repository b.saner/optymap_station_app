import { Injectable } from '@angular/core';
import {apiDev, info, login, otpvalidate, register} from '../../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {JwtHelperService} from '@auth0/angular-jwt';
import {MenuService} from "../menu.service";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient, private router: Router, private jwtHelper: JwtHelperService,private menuService: MenuService
  ) { }

  login(telephone: string, password: string): Observable<any> {
    return this.http.post(`${apiDev}${login}`, {telephone, password});
  }

  infouser(telephone: string): Observable<any> {
    return this.http.get(`${apiDev}${info}${telephone}`);
  }
  update(user, id): Observable<any> {
    return this.http.put(`${apiDev}${info}` + id, user);
  }
  valide(otp): Observable<any> {
    return this.http.get(`${apiDev}${otpvalidate}${otp}`);
  }

  register(user): Observable<any> {
    return this.http.post(`${apiDev}${register}`, user);
  }

  public logout() {
    localStorage.removeItem('token_access');
    localStorage.setItem('isLogged', 'false');
    // localStorage.removeItem('role');
    this.menuService.iAmlog({
      logged: 'true'
    });
    this.router.navigate(['login']);
    // window.location.reload();
  }


  public isTokenExpired(): boolean {
    return this.jwtHelper.isTokenExpired(this.jwtHelper.tokenGetter());
  }

  public isLoggedIn() {
    return localStorage.getItem('isLogged') === 'true';
  }

}
