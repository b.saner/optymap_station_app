import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ArretPageRoutingModule } from './arret-routing.module';

import { ArretPage } from './arret.page';
import {Ng2SearchPipeModule} from "ng2-search-filter";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ArretPageRoutingModule,
    Ng2SearchPipeModule
  ],
  declarations: [ArretPage]
})
export class ArretPageModule {}
