import { Component, OnInit } from '@angular/core';
import {NavigationExtras, Router} from '@angular/router';
import {MenuController, ModalController} from '@ionic/angular';
import {Station} from '../model/station';
import {Arret} from "../model/arret";
import {Line} from "../model/line";
import {OtherService} from "../services/otherservices/other.service";

@Component({
  selector: 'app-arret',
  templateUrl: './arret.page.html',
  styleUrls: ['./arret.page.scss'],
})
export class ArretPage implements OnInit {
  public id: number;
  public arrets: Arret;
  public lines: Line;
  public searchText: string;

  constructor(private router: Router, private modalController: ModalController, private menuController: MenuController,
              private otherService: OtherService) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      idarret: number;
    };
    if (state == null || state.idarret === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      this.id = state.idarret;
      console.log(this.id);
      this.infoarret();
    }
  }

  ngOnInit() {
  }
  menu() {
    this.menuController.toggle();
  }

  infoarret(){
    // this.otherService.arretbyid(this.id).subscribe(text => {
    //   console.log(text);
    //   this.arrets = text;
    // });

    this.otherService.linebyarretid(this.id).subscribe(lines =>{
      console.log(lines);
      this.lines = lines;
    });
  }

  gotoline(id: number) {
    const idline = id;
    const navigationExtras: NavigationExtras = {
      state: {idline}
    };
    this.router.navigate(['/detailline'], navigationExtras);

  }

  vocal() {

  }
}
