import { Component, OnInit } from '@angular/core';
import {Gab} from '../model/gab';
import {Router} from '@angular/router';
import {DetailstationService} from '../services/detailstation/detailstation.service';
import {Essence} from "../model/essence";

@Component({
  selector: 'app-essence',
  templateUrl: './essence.page.html',
  styleUrls: ['./essence.page.scss'],
})
export class EssencePage implements OnInit {

  public id: number;
  public essence: Essence;

  constructor(private router: Router, private stationservice: DetailstationService) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      idessence: number;
    };
    if (state == null || state.idessence === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      this.id = state.idessence;
      console.log(this.id);
    }
  }

  ngOnInit() {
    this.detailessence();
  }
  detailessence(){
    this.stationservice.essencebyid(this.id).subscribe(text => {
      console.log(text);
      this.essence = text;
    });
  }
}
