import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import Swal from 'sweetalert2';
import {UserService} from '../services/user/user.service';

@Injectable({
    providedIn: 'root'
})
export class
AuthGuardService implements CanActivate {

    constructor(private router: Router, private authService: UserService) {
    }

    // canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    //     if (this.authService.isLoggedIn) {
    //         return true;
    //     }
    //     // Navigate to the login page with extras
    //     this.router.navigate(['/auth'], {
    //         queryParams: {
    //             redirect: state.url
    //         }
    //     });
    //     return false;
    // }


    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (!this.authService.isLoggedIn()) {
            // Swal.fire('Not connected', 'Veuillez vous connecter afin d\'acceder a la ressource demandee!', 'error')
            //     .then(res => {
            //         if (res.value) {
                        this.router.navigate(['login'], {queryParams: {redirect: state.url}});
                        return false;
                //     }
                // });
        } else if (this.authService.isTokenExpired()) {
            Swal.fire('Session expirée', ' Veuillez vous reconnecter  SVP!', 'error')
                .then(res => {
                    if (res.value) {
                        this.router.navigate(['login'], {queryParams: {redirect: state.url}});
                        return false;
                    }
                });
        } else {
            return true;
        }
    }

}
