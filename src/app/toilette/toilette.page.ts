import { Component, OnInit } from '@angular/core';
import {Toilette} from '../model/toilette';
import {NavigationExtras, Router} from '@angular/router';
import {DetailstationService} from '../services/detailstation/detailstation.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-toilette',
  templateUrl: './toilette.page.html',
  styleUrls: ['./toilette.page.scss'],
})
export class ToilettePage implements OnInit {


  public id: number;
  private toilette: Toilette;

  constructor(private router: Router, private stationservice: DetailstationService) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      idtoilette: number;
    };
    if (state == null || state.idtoilette === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      this.id = state.idtoilette;
      console.log(this.id);
    }
  }

  ngOnInit() {
    this.detailtoilette();
  }
  detailtoilette(){
    this.stationservice.toilettebyid(this.id).subscribe(text => {
      console.log(text);
      this.toilette = text;
    });
  }

}
