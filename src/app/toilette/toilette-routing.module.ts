import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ToilettePage } from './toilette.page';

const routes: Routes = [
  {
    path: '',
    component: ToilettePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ToilettePageRoutingModule {}
