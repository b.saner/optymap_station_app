import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ToilettePageRoutingModule } from './toilette-routing.module';

import { ToilettePage } from './toilette.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ToilettePageRoutingModule
  ],
  declarations: [ToilettePage]
})
export class ToilettePageModule {}
