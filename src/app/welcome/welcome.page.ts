import {Component, OnInit, ViewChild} from '@angular/core';
import { IonSlides } from '@ionic/angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {retry} from 'rxjs/operators';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';
import {UserService} from '../services/user/user.service';
import {Utilisateur} from '../model/utilisateur';
import {RegionService} from '../services/region/region.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {
  public registerForm: FormGroup;
  public loading = false;
  showpassword = false;
  passwordToggle = 'eye';
  public submitted = false;
  public utilisateur: Utilisateur = new Utilisateur();
  slideOpts = {
    initialSlide: 0,
  };
@ViewChild('slides', {static: true}) slides: IonSlides;
  departements: any;
  regions: any;

  constructor(private formBuilder: FormBuilder, private router: Router,
              private authService: UserService, private regionService: RegionService) {

  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      telephone: ['', Validators.required],
      password: ['', Validators.required],
        type: [1, Validators.required],
        sexe: ['', Validators.required],
        prenom: ['', Validators.required],
        departement: ['', Validators.required],
        region: ['', Validators.required],
    });
    this.listregion();
    }
  gotoinscription(){
    this.slides.lockSwipes(false);
    this.slides.slideTo(3, 500);
    this.slides.lockSwipes(true); }

  next()  {
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true); }

  togglepassword(): void {
    this.showpassword = !this.showpassword;
    if (this.passwordToggle === 'eye')  {
      this.passwordToggle = 'eye-off';
    } else {
      this.passwordToggle = 'eye';
    }
  }
  get f() {
    return this.registerForm.controls;
  }
  onSubmit() {
    this.loading = true;
    console.log(this.utilisateur);
    this.authService.register(this.utilisateur)
        .pipe( retry(1))
        .subscribe(res => {
          Swal.fire({
            icon: 'success', title: 'Votre enregistrement c\'est bien passé', showConfirmButton: false, timer: 2500,
          });
          localStorage.setItem('token_access', res.token);
          localStorage.setItem('isLogged', 'false');
          localStorage.setItem('telephone', this.f.telephone.value);
          this.loading = false;
          this.router.navigate(['/login']);

        }, error => {
          this.loading = false;
          Swal.fire({
            icon: 'error', title: 'Votre enregistrement rencontre une erreur', showConfirmButton: false, timer: 2500,
          });
        });
  }
  listregion(){
    this.regionService.allregion()
        .pipe( retry(1))
        .subscribe(res => {
          this.regions = res;
          console.log(res);
        }, error => {
          console.log(error);
        });
  }
  departementid(id){
    this.regionService.departmentbyregion(id)
        .pipe( retry(1))
        .subscribe(res => {
          this.departements = res;
          console.log(res);
        }, error => {
          console.log(error);
        });
  }

    gotohome(){
        this.loading = true;
        this.utilisateur.telephone = 'anonyme';
        this.utilisateur.password = 'anonyme';
        this.utilisateur.nom = 'Anonyme';
        this.utilisateur.prenom = 'Anonyme';
        this.utilisateur.type = 1;
      localStorage.setItem('typeuser', '1');
      localStorage.setItem('nomuser', 'Anonyme');

      console.log(this.utilisateur);
        this.authService.register(this.utilisateur)
            .pipe( retry(1))
            .subscribe(res => {
                Swal.fire({
                    icon: 'success', title: 'Bienvenue dans OptyMap', showConfirmButton: false, timer: 2500,
                });
                localStorage.setItem('token_access', res.token);
                localStorage.setItem('isLogged', 'true');
                localStorage.setItem('iduser', res.id);
                this.loading = false;
              this.router.navigate(['/home']);

            }, error => {
                this.loading = false;
                Swal.fire({
                    icon: 'error', title: 'OptyMap a rencontré une erreur', showConfirmButton: false, timer: 2500,
                });
            });
        // localStorage.setItem('isLogged', 'true');

    }
    endapp(){
        navigator['app'].exitApp();
    }
}
