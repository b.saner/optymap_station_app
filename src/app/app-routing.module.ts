import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    // redirectTo: 'itineraire',
    // redirectTo: 'welcome',
    pathMatch: 'full'
  },
  {
    path: 'detailstation',
    loadChildren: () => import('./detailstation/detailstation.module').then( m => m.DetailstationPageModule)
  },
  {
    path: 'essence',
    loadChildren: () => import('./essence/essence.module').then( m => m.EssencePageModule)
  },
  {
    path: 'gazoil',
    loadChildren: () => import('./gazoil/gazoil.module').then( m => m.GazoilPageModule)
  },
  {
    path: 'electrique',
    loadChildren: () => import('./electrique/electrique.module').then( m => m.ElectriquePageModule)
  },
  {
    path: 'superette',
    loadChildren: () => import('./superette/superette.module').then( m => m.SuperettePageModule)
  },
  {
    path: 'gab',
    loadChildren: () => import('./gab/gab.module').then( m => m.GabPageModule)
  },
  {
    path: 'repos',
    loadChildren: () => import('./repos/repos.module').then( m => m.ReposPageModule)
  },
  {
    path: 'gaz',
    loadChildren: () => import('./gaz/gaz.module').then( m => m.GazPageModule)
  },
  {
    path: 'culte',
    loadChildren: () => import('./culte/culte.module').then( m => m.CultePageModule)
  },
  {
    path: 'entretien',
    loadChildren: () => import('./entretien/entretien.module').then( m => m.EntretienPageModule)
  },
  {
    path: 'e-money',
    loadChildren: () => import('./e-money/e-money.module').then( m => m.EMoneyPageModule)
  },
  {
    path: 'term',
    loadChildren: () => import('./term/term.module').then( m => m.TermPageModule)
  },
  {
    path: 'faq',
    loadChildren: () => import('./faq/faq.module').then( m => m.FaqPageModule)
  },
  {
    path: 'privacy',
    loadChildren: () => import('./privacy/privacy.module').then( m => m.PrivacyPageModule)
  },
  {
    path: 'support',
    loadChildren: () => import('./support/support.module').then( m => m.SupportPageModule)
  },
  {
    path: 'setting',
    loadChildren: () => import('./setting/setting.module').then( m => m.SettingPageModule)
  },
  {
    path: 'gpl',
    loadChildren: () => import('./gpl/gpl.module').then( m => m.GplPageModule)
  },
  {
    path: 'addstation',
    loadChildren: () => import('./addstation/addstation.module').then( m => m.AddstationPageModule)
  },
  {
    path: 'toilette',
    loadChildren: () => import('./toilette/toilette.module').then( m => m.ToilettePageModule)
  },
  {
    path: 'manager',
    loadChildren: () => import('./manager/manager.module').then( m => m.ManagerPageModule)
  },
  {
    path: 'mapone',
    loadChildren: () => import('./mapone/mapone.module').then( m => m.MaponePageModule)
  },
  {
    path: 'itineraire',
    loadChildren: () => import('./itineraire/itineraire.module').then( m => m.ItinerairePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'welcome',
    loadChildren: () => import('./welcome/welcome.module').then( m => m.WelcomePageModule)
  },
  {
    path: 'detailmecanicien',
    loadChildren: () => import('./detailmecanicien/detailmecanicien.module').then( m => m.DetailmecanicienPageModule)
  },
  {
    path: 'addgaragiste',
    loadChildren: () => import('./addgaragiste/addgaragiste.module').then( m => m.AddgaragistePageModule)
  },
  {
    path: 'myaccount',
    loadChildren: () => import('./myaccount/myaccount.module').then( m => m.MyaccountPageModule)
  },
  {
    path: 'rating',
    loadChildren: () => import('./rating/rating.module').then( m => m.RatingPageModule)
  },
  {
    path: 'signaler',
    loadChildren: () => import('./signaler/signaler.module').then( m => m.SignalerPageModule)
  },
  {
    path: 'addpharmacie',
    loadChildren: () => import('./addpharmacie/addpharmacie.module').then( m => m.AddpharmaciePageModule)
  },
  {
    path: 'detailsignalisation',
    loadChildren: () => import('./detailsignalisation/detailsignalisation.module').then( m => m.DetailsignalisationPageModule)
  },
  {
    path: 'detailpharmacie',
    loadChildren: () => import('./detailpharmacie/detailpharmacie.module').then( m => m.DetailpharmaciePageModule)
  },
  {
    path: 'detailpopup',
    loadChildren: () => import('./detailpopup/detailpopup.module').then( m => m.DetailpopupPageModule)
  },
  {
    path: 'detailline',
    loadChildren: () => import('./detailline/detailline.module').then( m => m.DetaillinePageModule)
  },
  {
    path: 'arret',
    loadChildren: () => import('./arret/arret.module').then( m => m.ArretPageModule)
  },
  {
    path: 'detailvulcanisateur',
    loadChildren: () => import('./detailvulcanisateur/detailvulcanisateur.module').then( m => m.DetailvulcanisateurPageModule)
  },
  {
    path: 'detailhopital',
    loadChildren: () => import('./detailhopital/detailhopital.module').then( m => m.DetailhopitalPageModule)
  },
  {
    path: 'popupcall',
    loadChildren: () => import('./popupcall/popupcall.module').then( m => m.PopupcallPageModule)
  },
  {
    path: 'mapsetting',
    loadChildren: () => import('./mapsetting/mapsetting.module').then( m => m.MapsettingPageModule)
  },
  {
    path: 'detailgendarmerie',
    loadChildren: () => import('./detailgendarmerie/detailgendarmerie.module').then( m => m.DetailgendarmeriePageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),

  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
