import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailhopitalPage } from './detailhopital.page';

const routes: Routes = [
  {
    path: '',
    component: DetailhopitalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailhopitalPageRoutingModule {}
