import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailhopitalPageRoutingModule } from './detailhopital-routing.module';

import { DetailhopitalPage } from './detailhopital.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailhopitalPageRoutingModule
  ],
  declarations: [DetailhopitalPage]
})
export class DetailhopitalPageModule {}
