import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UserService} from '../services/user/user.service';
import {retry} from 'rxjs/operators';
import Swal from 'sweetalert2';
import {Platform} from '@ionic/angular';
import {MenuService} from '../services/menu.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  public registerForm: FormGroup;
  showpassword = false;
  public submitted = false;
  public loading = false;
  passwordToggle = 'eye';

  constructor(private formBuilder: FormBuilder, private router: Router, private authService: UserService,
              public platform: Platform, private menuService: MenuService) {
  }

  get f() {
    return this.registerForm.controls;
  }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      telephone: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  togglepassword(): void {
    this.showpassword = !this.showpassword;
    if (this.passwordToggle === 'eye') {
      this.passwordToggle = 'eye-off';
    } else {
      this.passwordToggle = 'eye';
    }
  }

  public async onSubmit() {
    this.loading = true;
    this.authService.login(this.f.telephone.value, this.f.password.value)
      .pipe(retry(1))
      .subscribe(res => {
        localStorage.setItem('token_access', res.token);
        localStorage.setItem('isLogged', 'true');
        this.authService.infouser(this.f.telephone.value).subscribe(usersinfo => {
          localStorage.setItem('iduser', usersinfo.id);
          localStorage.setItem('typeuser', usersinfo.type);
        });
        localStorage.setItem('telephone', this.f.telephone.value);
        this.menuService.iAmlog({
          logged: 'true'
        });
        this.router.navigate(['/home']);
        // window.location.reload();
        this.loading = false;

      }, error => {
        this.loading = false;
        Swal.fire({
          icon: 'error', title: 'Verifier vos identifiants ', showConfirmButton: false, timer: 2500,
        });
      });
  }


}
