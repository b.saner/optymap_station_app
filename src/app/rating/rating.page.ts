import {Component, Input, OnInit} from '@angular/core';
import {Review} from '../model/review';
import {ReviewService} from '../services/review/review.service';
import {Utilisateur} from '../model/utilisateur';
import {ModalController} from '@ionic/angular';
import {Mecanicien} from '../model/mecanicien';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.page.html',
  styleUrls: ['./rating.page.scss'],
})
export class RatingPage implements OnInit {
  @Input() id: number;
  public review2: Review;
  public ratingstart: any;
  public anonyme = true;

  constructor(private modalController: ModalController, private ratingService: ReviewService) {
    this.review2 = new Review();
    this.review2.reviewer = new Utilisateur();
    this.review2.mecanicien = new Mecanicien();
  }

  ngOnInit() {
    console.log(this.id);


    // eslint-disable-next-line radix
    this.review2.reviewer.id = parseInt(localStorage.getItem('iduser'));
    // eslint-disable-next-line radix
    this.review2.mecanicien.id = this.id;
    this.myrating();
  }
  myrating(){
    this.ratingService.myreviewmecanicienbyuser(this.review2.reviewer.id,this.id).subscribe(rating => {
      console.log(rating);
      if (rating != null) {
        this.review2 = rating;
        this.ratingstart = this.review2.rating;

      } else {
        this.review2.rating = this.ratingstart = 3;
      } });
  }
  dismiss() {
    this.modalController.dismiss({
      dismissed: true
    });
  }
  ratingChange2(rating){
    console.log('changed rating: ', rating);
    this.ratingstart = rating;
    // do your stuff
  }

  onSubmit2() {
    console.log(this.anonyme);
    if (this.anonyme==true){
      this.review2.nom = 'anonyme';
    }
    else if(this.anonyme==false) {
      this.review2.nom = localStorage.getItem('nomuser');
    }
    else {
      this.review2.nom = 'anonyme';
    }
    // this.review.huissier.id = parseInt(localStorage.getItem('huissierid'));
    console.log(this.review2);
    this.review2.rating = this.ratingstart;
    this.ratingService.rating(this.review2).subscribe(infosignal => {
      console.log(infosignal);
      Swal.fire({
        icon: 'success',
        // title: 'Merci de votre signalisation',
        html: '<p style="font-size: 15px">Le mecanicien a été noté</p>',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
      });
      // Swal.fire({
      //     icon: 'warning',
      //     showCancelButton: true,
      //     // title: 'Enregistrement',
      //     html: '<p style="font-size: 15px">Pour une meilleur prise en charge, enregistrez vous</p>',
      //     showConfirmButton: false,
      //     timer: 3000,
      //     timerProgressBar: true,
      // }).then((result) => {
      //     if (result.isConfirmed) {
      //     this.router.navigate(['/register']);
      //     }
      // });
      this.dismiss();
    }, error => {
      Swal.fire({
        icon: 'error',
        // title: 'Merci de votre signalisation',
        html: '<p style="font-size: 15px">Le mecanicien n\'a pas été noté</p>',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,

      });
    });
  }
}
