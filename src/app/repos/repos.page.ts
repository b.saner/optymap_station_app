import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {DetailstationService} from '../services/detailstation/detailstation.service';
import {Airederepos} from '../model/airederepos';

@Component({
  selector: 'app-repos',
  templateUrl: './repos.page.html',
  styleUrls: ['./repos.page.scss'],
})
export class ReposPage implements OnInit {
  slideOpts = {
    initialSlide: 0,
    speed: 400,
    autoplay: {
      delay: 3000,
      disableOnInteraction: false
    },
    pagination:{
      clickable: true
    },
    navigation:true
  };
  public id: number;
  private aire: Airederepos;

  constructor(private router: Router,private stationservice: DetailstationService) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      idaire: number;
    };
    if (state == null || state.idaire === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      this.id = state.idaire;
      console.log(this.id);
    }
  }

  ngOnInit() {
    this.detailaire();
  }
  detailaire(){
    this.stationservice.airedereposbyid(this.id).subscribe(text => {
      console.log(text);
      this.aire = text;
    });
  }

}
