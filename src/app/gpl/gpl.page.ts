import { Component, OnInit } from '@angular/core';
import {Gpl} from '../model/gpl';
import {Router} from '@angular/router';
import {DetailstationService} from '../services/detailstation/detailstation.service';

@Component({
  selector: 'app-gpl',
  templateUrl: './gpl.page.html',
  styleUrls: ['./gpl.page.scss'],
})
export class GplPage implements OnInit {
  public id: number;
  private gpl: Gpl;
  constructor(private router: Router, private stationservice: DetailstationService) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      idgpl: number;
    };
    if (state == null || state.idgpl === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      this.id = state.idgpl;
      console.log(this.id);
    }
  }

  ngOnInit() {
    this.detailgpl();
  }

  detailgpl(){
    this.stationservice.gplbyid(this.id).subscribe(text => {
      console.log(text);
      this.gpl = text;
    });
  }

}
