import { Component, OnInit } from '@angular/core';
import Swal from "sweetalert2";
import {MaponePage} from "../mapone/mapone.page";
import {Mecanicien} from "../model/mecanicien";
import {Review} from "../model/review";
import {Vulcanisateur} from "../model/vulcanisateur";
import {Router} from "@angular/router";
import {CallNumber} from "@ionic-native/call-number/ngx";
import {OtherService} from "../services/otherservices/other.service";
import {RegionService} from "../services/region/region.service";
import {ModalController} from "@ionic/angular";
import {Utilisateur} from "../model/utilisateur";
import {PopupcallPage} from "../popupcall/popupcall.page";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-detailvulcanisateur',
  templateUrl: './detailvulcanisateur.page.html',
  styleUrls: ['./detailvulcanisateur.page.scss'],
})
export class DetailvulcanisateurPage implements OnInit {
  public id: number;
  public vulcanisateurs: Vulcanisateur;
  public departement: any;
  language: string;

  constructor(private router: Router, private callNumber: CallNumber, private otherservice: OtherService,
              private regionservice: RegionService, private modalController: ModalController, private translateService: TranslateService)  {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      idvulcanisateur: number;
    };
    if (state == null || state.idvulcanisateur === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      console.log(state);
      this.id = state.idvulcanisateur;
      this.vulcanisateurs= new Vulcanisateur();
    }

  }

  ngOnInit() {
    if (localStorage.getItem('lang') ) {
      this.language = (localStorage.getItem('lang'));
      this.translateService.use(this.language);
    }
    this.detailvulcanisateur();
  }

  detaildepartement(id){
    console.log(id);
    this.regionservice.departmentbyid( parseInt(id)).subscribe(text => {
      console.log(text);
      this.departement = text;
    });
  }

  detailvulcanisateur(){
    this.otherservice.vulcanisateurbyid(this.id).subscribe(text => {
      console.log(text);
      this.vulcanisateurs = text;
      this.detaildepartement(text.departement);

    });
  }
  statutvulcanisateur(statut) {
    if (statut == 0){
      Swal.fire({
        icon: 'error', title: 'Le service est indisponible', showConfirmButton: false, timer: 3000,
      });
    }
    else if (statut == 3){
      Swal.fire({
        icon: 'warning', title: 'Aucune information sur ce service', showConfirmButton: false, timer: 3000,
      });
    }
    else if (statut == 2){
      Swal.fire({
        icon: 'error', title: 'Ce service est en panne', showConfirmButton: false, timer: 3000,
      });
    }
    else {
      Swal.fire({
        icon: 'success', title: 'Ce service est disponible', showConfirmButton: false, timer: 3000,
      });}
  }

  appel(numero) {
    if (numero === 'none'){
      Swal.fire({
        icon: 'error', title: 'Le numéro est indisponible', showConfirmButton: false, timer: 3000,
      });
    }
    else {

      this.callNumber.callNumber(numero, true)
        .then(res =>{ console.log('Launched dialer!', res);
          this.otherservice.callclick(this.id,this.vulcanisateurs).subscribe(call => {
            console.log(call);
            this.detailvulcanisateur();
          });})
        .catch(err => console.log('Error launching dialer', err));
    }
  }

  async emplacement(id) {
    const signalerModal = await this.modalController.create({
      component: MaponePage,
      cssClass: 'my-custom-class',
      swipeToClose: true, componentProps: {
        id
      }

    });
    return await signalerModal.present();

  }

  async infocall(tel: string) {
    if (tel == null || tel =='none' ||tel ==undefined){
      Swal.fire({
        icon: 'error', title: 'Le numéro est indisponible', showConfirmButton: false, timer: 2500,
      });
    }
    else{
      const callModal = await this.modalController.create({
        component: PopupcallPage,
        cssClass: 'cmodal',
        swipeToClose: true, componentProps: {
          tel
        }
      });
      return await callModal.present();
    }
  }

}
