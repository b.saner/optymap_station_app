import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailvulcanisateurPage } from './detailvulcanisateur.page';

const routes: Routes = [
  {
    path: '',
    component: DetailvulcanisateurPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailvulcanisateurPageRoutingModule {}
