import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailvulcanisateurPageRoutingModule } from './detailvulcanisateur-routing.module';

import { DetailvulcanisateurPage } from './detailvulcanisateur.page';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailvulcanisateurPageRoutingModule,
    TranslateModule
  ],
  declarations: [DetailvulcanisateurPage]
})
export class DetailvulcanisateurPageModule {}
