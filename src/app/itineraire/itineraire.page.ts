import { Component,Input, OnInit } from '@angular/core';
import {Map, tileLayer, marker, icon, circle, polygon, geoJSON, Marker} from 'leaflet';
import 'leaflet-routing-machine';
import 'leaflet-control-geocoder';
import * as L1 from 'leaflet.markercluster';
// import * as L from 'leaflet';
declare const L: any;
import  'leaflet.locatecontrol';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import {ModalController} from '@ionic/angular';
import {NavigationExtras, Router} from '@angular/router';
import {StationService} from '../services/station/station.service';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import {NominatimService} from '../services/nominatim/nominatim-service';

const meicon = L.icon({
  iconUrl: '../assets/icon-map.png',
  iconSize: [25, 41],
  iconAnchor: [10, 41],
  popupAnchor: [2, -45],});

@Component({
  selector: 'app-itineraire',
  templateUrl: './itineraire.page.html',
  styleUrls: ['./itineraire.page.scss'],
})
export class ItinerairePage implements OnInit {

  map: Map;
  newMarker: any;
  newMarker2: any;
  public locations: any;
  @Input() id: string;
  public latlong: any;
  public debut: any;
  public fin: any;
   options: NativeGeocoderOptions = {
    useLocale: false,
    maxResults: 5
  };
  constructor(private geolocation: Geolocation, public modalController: ModalController,private adresseservice: NominatimService,
              private mapservice: StationService, private router: Router,private nativeGeocoder: NativeGeocoder) { }


  ngOnInit() {
  }
  ionViewDidEnter() {
    this.map = new Map('mapIditin');
    tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',

    ).addTo(this.map);
    // }
    this.locatePosition();
    // geocoderControl.addTo(this.map);
    // geocoderControl.on('markgeocode', f => {
    //   console.log(f);
    // });
  }
  locatePosition() {
    this.map.locate({ watch: false, setView: false, maxZoom: 12, enableHighAccuracy : true}).on('locationfound', (e: any) => {
      console.log('location', e);
      this.latlong = e;
      // Si la localisation est disponible on integrera notre position
      // this.newMarker = marker([e.latitude, e.longitude], {
      //   draggable: false,
      //   icon: meicon,
      //
      // }).addTo(this.map);
      // this.newMarker.bindPopup('<p>Ma position</p>');
      this.newMarker2 = marker([e.latitude, e.longitude]).addTo(this.map);
      L.Routing.control({
        waypoints: [
          L.latLng( e.latitude, e.longitude),
          L.latLng(57.6792, 11.949)
        ],
        routeWhileDragging: true,
        geocoder: L.Control.Geocoder.nominatim({}),

        show: false,
      }).addTo(this.map);
    });

  }
  locatePositionpres() {
    console.log('test');
    this.map.setView(this.latlong, 16);

    //   this.map.locate().on('locationfound', (e: any) => {
    //   console.log('location', e);
    //   // Si la localisation est disponible on integrera notre position
    //   // this.newMarker = marker([e.latitude, e.longitude], {
    //   //   draggable: false,
    //   //   icon: meicon,
    //   //
    //   // }).addTo(this.map);
    //   // this.newMarker.bindPopup('<p>Ma position</p>');
    // });
  }
  // ittineraire(){
  //   console.log(this.id);
  //
  //   this.mapservice.stationbyid(this.id).subscribe(positions => {
  //     this.locations = positions;
  //     console.log(positions);
  //     console.log(positions.length);
  //     L.marker([positions.latitude, positions.longitude])
  //       .addTo(this.map)
  //
  //       // tslint:disable-next-line:only-arrow-functions
  //       .on('click', event =>  {
  //         // this.router.navigate(['/signalisationdetail'], navigationExtras);
  //
  //       });
  //     L.Routing.control({
  //       router: L.Routing.osrmv1({
  //         serviceUrl: `http://router.project-osrm.org/route/v1/`,
  //         language: 'fr',
  //         profile: 'car'
  //       }),
  //       showAlternatives: true,
  //       fitSelectedRoutes: 'smart',
  //       routeWhileDragging: false,
  //       show: false,
  //       waypoints: [
  //         // L.latLng(57.74, 11.94),
  //         // L.latLng(57.6792, 11.949)
  //         L.latLng(positions.latitude, positions.longitude),
  //         L.latLng(this.latlong.lat, this.latlong.lng)
  //       ]
  //     }).addTo(this.map);
  //     // Lr.Routing.control({
  //     //   waypoints: [
  //     //     L.latLng(positions.latitude, positions.longitude),
  //     //     L.latLng(positions.latitude+1, positions.longitude+1)
  //     //     // L.latLng(this.latlong.latlng, this.latlong.lng)
  //     //   ], router: new Lr.Routing.osrmv1({
  //     //     language: 'fr',
  //     //     profile: 'car'
  //     //   }),
  //     //   geocoder: Lr.Control.Geocoder.nominatim({})
  //
  //     // }).addTo(this.map);
  //   });
  // }

  search()
  {
    console.log(this.debut);
  this.adresseservice.addressLook(this.debut).subscribe(response =>{
    console.log(response);
  });
  }
  ionViewDidLeave() {
    this.map.remove();
  }

}
