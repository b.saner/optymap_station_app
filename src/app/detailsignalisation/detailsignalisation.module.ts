import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailsignalisationPageRoutingModule } from './detailsignalisation-routing.module';

import { DetailsignalisationPage } from './detailsignalisation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailsignalisationPageRoutingModule
  ],
  declarations: [DetailsignalisationPage]
})
export class DetailsignalisationPageModule {}
