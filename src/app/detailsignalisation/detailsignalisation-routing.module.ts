import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailsignalisationPage } from './detailsignalisation.page';

const routes: Routes = [
  {
    path: '',
    component: DetailsignalisationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailsignalisationPageRoutingModule {}
