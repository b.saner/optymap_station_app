import { Component, OnInit } from '@angular/core';
import {Pharmacie} from '../model/pharmacie';
import {Router} from '@angular/router';
import {CallNumber} from '@ionic-native/call-number/ngx';
import {OtherService} from '../services/otherservices/other.service';
import {ModalController} from '@ionic/angular';
import {ReviewService} from '../services/review/review.service';
import {Signalisation} from '../model/signalisation';
import Swal from 'sweetalert2';
import {MaponePage} from '../mapone/mapone.page';

@Component({
  selector: 'app-detailsignalisation',
  templateUrl: './detailsignalisation.page.html',
  styleUrls: ['./detailsignalisation.page.scss'],
})
export class DetailsignalisationPage implements OnInit {
  public id: number;
  public signalisations: Signalisation;
  constructor(private router: Router, private callNumber: CallNumber, private otherService: OtherService,
              private modalController: ModalController, private reviewService: ReviewService,
  ) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      idsignalisation: number;
    };
    if (state == null || state.idsignalisation === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      console.log(state);
      this.id = state.idsignalisation;
      this.signalisations= new Signalisation();

    }
  }
  ngOnInit() {
  }

  detailsignalisation(){
    this.otherService.signalisationbyid(this.id).subscribe(text => {
      console.log(text);
      this.signalisations = text;
    });
  }


  appel(numero) {
    if (numero === 'none'){
      Swal.fire({
        icon: 'error', title: 'Le numéro est indisponible', showConfirmButton: false, timer: 3000,
      });
    }
    else {

      this.callNumber.callNumber(numero, true)
        .then(res =>{ console.log('Launched dialer!', res);
          this.otherService.callclick(this.id,this.signalisations).subscribe(call => {
            console.log(call);
            this.detailsignalisation();
          });})
        .catch(err => console.log('Error launching dialer', err));
    }
  }

  async emplacement(id) {
    const signalerModal = await this.modalController.create({
      component: MaponePage,
      cssClass: 'my-custom-class',
      swipeToClose: true, componentProps: {
        id
      }

    });
    return await signalerModal.present();

  }
}
