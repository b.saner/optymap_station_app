import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailgendarmeriePage } from './detailgendarmerie.page';

const routes: Routes = [
  {
    path: '',
    component: DetailgendarmeriePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailgendarmeriePageRoutingModule {}
