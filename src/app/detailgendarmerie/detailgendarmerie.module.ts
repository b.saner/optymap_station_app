import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailgendarmeriePageRoutingModule } from './detailgendarmerie-routing.module';

import { DetailgendarmeriePage } from './detailgendarmerie.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailgendarmeriePageRoutingModule
  ],
  declarations: [DetailgendarmeriePage]
})
export class DetailgendarmeriePageModule {}
