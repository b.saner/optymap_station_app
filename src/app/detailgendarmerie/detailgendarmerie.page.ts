import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import {MaponePage} from '../mapone/mapone.page';
import {Router} from '@angular/router';
import {CallNumber} from '@ionic-native/call-number/ngx';
import {Gendarmerie} from "../model/gendarmerie";
import {OtherService} from "../services/otherservices/other.service";
import {RegionService} from "../services/region/region.service";
import {ModalController} from "@ionic/angular";
import {PopupcallPage} from "../popupcall/popupcall.page";
@Component({
  selector: 'app-detailgendarmerie',
  templateUrl: './detailgendarmerie.page.html',
  styleUrls: ['./detailgendarmerie.page.scss'],
})
export class DetailgendarmeriePage implements OnInit {
  public id: number;
  public gendarmeries: Gendarmerie;
  public departement: any;
  constructor(private router: Router, private callNumber: CallNumber, private otherservice: OtherService,
              private regionservice: RegionService, private modalController: ModalController) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      idgendarmerie: number;
    };
    if (state == null || state.idgendarmerie === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      console.log(state);
      this.id = state.idgendarmerie;
      this.gendarmeries= new Gendarmerie();
    }
  }

  ngOnInit() {
    this.detailgendarmeries();
  }

  detaildepartement(id){
    console.log(id);
    this.regionservice.departmentbyid( parseInt(id)).subscribe(text => {
      console.log(text);
      this.departement = text;
    });
  }

  detailgendarmeries(){
    this.otherservice.gendarmeriebyid(this.id).subscribe(text => {
      console.log(text);
      this.gendarmeries = text;
      this.detaildepartement(text.departement);
    });
  }

  appel(numero) {
    if (numero === 'none'){
      Swal.fire({
        icon: 'error', title: 'Le numéro est indisponible', showConfirmButton: false, timer: 3000,
      });
    }
    else {

      this.callNumber.callNumber(numero, true)
        .then(res =>{ console.log('Launched dialer!', res);
          this.otherservice.callclick(this.id,this.gendarmeries).subscribe(call => {
            console.log(call);
            this.detailgendarmeries();
          });})
        .catch(err => console.log('Error launching dialer', err));
    }
  }
  statutgendarmerie(statut) {
    if (statut == false){
      Swal.fire({
        icon: 'error', title: 'La gendarmerie est indisponible', showConfirmButton: false, timer: 3000,
      });
    }

    else {
      Swal.fire({
        icon: 'success', title: 'La gendarmerie est disponible', showConfirmButton: false, timer: 3000,
      });}
  }

  async emplacement(id) {
    const signalerModal = await this.modalController.create({
      component: MaponePage,
      cssClass: 'my-custom-class',
      swipeToClose: true, componentProps: {
        id
      }

    });
    return await signalerModal.present();

  }
  async infocall(tel: string) {
    if (tel == null || tel =='none' ||tel ==undefined){
      Swal.fire({
        icon: 'error', title: 'Le numéro est indisponible', showConfirmButton: false, timer: 2500,
      });
    }
    else{
      const callModal = await this.modalController.create({
        component: PopupcallPage,
        cssClass: 'cmodal',
        swipeToClose: true, componentProps: {
          tel
        }
      });
      return await callModal.present();
    }
  }

}
