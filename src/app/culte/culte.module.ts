import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CultePageRoutingModule } from './culte-routing.module';

import { CultePage } from './culte.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CultePageRoutingModule
  ],
  declarations: [CultePage]
})
export class CultePageModule {}
