import { Component, OnInit } from '@angular/core';
import {Culte} from "../model/culte";
import {Router} from "@angular/router";
import {DetailstationService} from "../services/detailstation/detailstation.service";

@Component({
  selector: 'app-culte',
  templateUrl: './culte.page.html',
  styleUrls: ['./culte.page.scss'],
})
export class CultePage implements OnInit {
  slideOpts = {
    initialSlide: 0,
    speed: 400,
    autoplay: {
      delay: 3000,
      disableOnInteraction: false
    },
    pagination:{
    clickable: true
  },
  navigation:true
  };

  public id: number;
  public culte: Culte;

  constructor(private router: Router, private stationservice: DetailstationService) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      idculte: number;
    };
    if (state == null || state.idculte === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      this.id = state.idculte;
      console.log(this.id);
    }
  }

  ngOnInit() {
    this.detailculte();
  }
  detailculte(){
    this.stationservice.cultebyid(this.id).subscribe(text => {
      console.log(text);
      this.culte = text;
    });
  }
}
