import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapsettingPageRoutingModule } from './mapsetting-routing.module';

import { MapsettingPage } from './mapsetting.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MapsettingPageRoutingModule
  ],
  declarations: [MapsettingPage]
})
export class MapsettingPageModule {}
