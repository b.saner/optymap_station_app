import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ModalController, NavParams} from "@ionic/angular";
import {MenuService} from "../services/menu.service";

@Component({
  selector: 'app-mapsetting',
  templateUrl: './mapsetting.page.html',
  styleUrls: ['./mapsetting.page.scss'],
})
export class MapsettingPage implements OnInit {
  styleMap: string;

  constructor( private modalController: ModalController, private globalMenuService: MenuService, navParams: NavParams) {
    console.log(navParams.get('styleMap'));
    this.styleMap = navParams.get('styleMap');
  }

  ngOnInit() {
  }
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true,
      test : 'test dismiss ok'
    });

  }

  segmentChanged(maptype) {
    this.globalMenuService.mystyleset(maptype.detail);

  }
}
