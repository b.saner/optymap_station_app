import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddpharmaciePage } from './addpharmacie.page';

const routes: Routes = [
  {
    path: '',
    component: AddpharmaciePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddpharmaciePageRoutingModule {}
