import {Component, OnInit, ViewChild} from '@angular/core';
import {ActionSheetController, IonSlides, Platform, ModalController} from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import Swal from 'sweetalert2';
import {NavigationExtras, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators, FormControl, FormArray} from '@angular/forms';
import * as L from 'leaflet';
import {Map, tileLayer, marker, icon, circle, polygon, geoJSON, Marker} from 'leaflet';
import {RegionService} from '../services/region/region.service';
import {retry} from 'rxjs/operators';
import {MecanicienService} from '../services/mecanicien/mecanicien.service';
import {OtherService} from '../services/otherservices/other.service';
import {Pharmacie} from '../model/pharmacie';
import {Manager} from '../model/manager';

const meicon = L.icon({
  iconUrl: '../assets/icon/pharmacie.png',
  iconSize: [30, 41],
  iconAnchor: [10, 41],
  popupAnchor: [2, -45], });

@Component({
  selector: 'app-addpharmacie',
  templateUrl: './addpharmacie.page.html',
  styleUrls: ['./addpharmacie.page.scss'],
})
export class AddpharmaciePage implements OnInit {

  @ViewChild('slides', {static: true}) slides: IonSlides;

  public mylatitude: any;
  public mylongitude: any;
  public type: number;
  public motif: string;
  public map: Map;
  newMarker: any;
  public latlong: any;
  departements: any;
  regions: any;
  slideOpts = {
    initialSlide: 0,
  };
  public pharmacie: Pharmacie;
  public slideNameForm: FormGroup;
  public slideContactForm: FormGroup;
  public slideRegionForm: FormGroup;
  public slidePositionForm: FormGroup;
  public slideServiceForm: FormGroup;

  constructor(public formBuilder: FormBuilder, private router: Router,
              private geolocation: Geolocation, private actionSheetController: ActionSheetController,
              private regionService: RegionService, private otherService: OtherService) {
    this.slideNameForm = formBuilder.group({
    nom: [''],
    pmanager: [''],
    nmanager: [''],
    type: [''],
  });

    this.slideContactForm = formBuilder.group({
      tel: [''],
      mail: [''],
    });

    this.slideRegionForm = formBuilder.group({
      region: [''],
      departement: [''],
      localite: [''],
    });

    this.slidePositionForm = formBuilder.group({
    });

    this.slideServiceForm = formBuilder.group({
      mecanique: [''],
      electrique: [''],
      carosserie: [''],
      urgence: [''],
      vdiagnostic: [''],
    });
  }

  ngOnInit() {
      this.pharmacie = new Pharmacie();
      this.pharmacie.manager = new Manager();
    this.listregion();
    this.slides.lockSwipes(true);
  }

  next() {
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true);
  }

  previous() {
    this.slides.lockSwipes(false);
    this.slides.slidePrev();
    this.slides.lockSwipes(true);
  }

  locatePosition() {
    this.map.locate({ watch: false, setView: true, maxZoom: 15, enableHighAccuracy : true}).on('locationfound', (e: any) => {
      console.log('location', e);
      this.latlong = e.latlng;
      //  const navigationExtras: NavigationExtras = {
      //   state: {id : "1" }
      // };
      // Si la localisation est disponible on integrera notre position
      this.newMarker = marker([e.latitude, e.longitude], {
        draggable: true,
        icon: meicon,
      })
      //   .on('click', event =>  {
      //   this.router.navigate(['/detailstation'], navigationExtras);
      // })
        .addTo(this.map);
      this.newMarker.on('dragend', f => {
        this.pharmacie.latitude = f.target.getLatLng().lat.toString();
        this.pharmacie.longitude = f.target.getLatLng().lng.toString();
      });

      // this.newMarker.bindPopup('<p>Ma position</p>');
      // this.newMarker2 = marker([e.latitude, e.longitude], animatedCircleIcon).addTo(this.map);
      // });
      this.pharmacie.latitude = e.latitude;
      this.pharmacie.longitude = e.longitude;
    });
  }

  ionViewDidEnter() {
    this.map = new Map('mapmec');
    tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',

    ).addTo(this.map);
    // }
    this.locatePosition();
  }

  listregion(){
    this.regionService.allregion()
      .pipe( retry(1))
      .subscribe(res => {
        this.regions = res;
        console.log(res);
      }, error => {
        console.log(error);
      });
  }

  departementid(id){
    this.regionService.departmentbyregion(id)
      .pipe( retry(1))
      .subscribe(res => {
        this.departements = res;
        console.log(res);
      }, error => {
        console.log(error);
      });
  }

  ionViewDidLeave() {
    this.map.remove();
  }

  onSubmit() {
    console.log(this.pharmacie);

    this.otherService.addpharmacie(this.pharmacie).subscribe(pharmacie => {
      console.log(pharmacie);
      Swal.fire({
        icon: 'success', title: 'Vous avez été enregistré comme pharmacie  et êtes en attente de validation',
        showConfirmButton: false, timer: 3000,
      });
      console.log(this.pharmacie);
    }, error => {
      Swal.fire({
        icon: 'error', title: 'Une erreur c\'est produit lors de l\'ajout !', showConfirmButton: false, timer: 3000,
      });    });
    this.router.navigate(['/home']);

  }

}
