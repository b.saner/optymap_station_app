import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddpharmaciePageRoutingModule } from './addpharmacie-routing.module';

import { AddpharmaciePage } from './addpharmacie.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddpharmaciePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [AddpharmaciePage]
})
export class AddpharmaciePageModule {}
