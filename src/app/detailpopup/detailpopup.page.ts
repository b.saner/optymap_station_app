import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {StationService} from '../services/station/station.service';
import {NavigationExtras, Router} from '@angular/router';
import {OtherService} from '../services/otherservices/other.service';
import {MecanicienService} from '../services/mecanicien/mecanicien.service';
import {ReviewService} from '../services/review/review.service';

@Component({
  selector: 'app-detailpopup',
  templateUrl: './detailpopup.page.html',
  styleUrls: ['./detailpopup.page.scss'],
})
export class DetailpopupPage implements OnInit {
  @Input() id: number;
  @Input() type: string;
  private stations: any;
  private pharmacies: any;
  private mecaniciens: any;
  private signalisations: any;
  private starRating= 0;
  private arrets: any;
  private lines: any;
  private oranges: any;
  private vulcanisateurs: any;
  private hopitals: any;
  private gendarmeries: any;

  constructor( public modalController: ModalController,private router: Router,private stationservice: StationService,
               private otherService: OtherService, private mecanicienservice: MecanicienService, private reviewService: ReviewService) { }

  ngOnInit() {
    console.log(this.id, this.type);
    this.infopop();
  }
  infopop(){
    if (this.type === 'station'){
      this.stationservice.stationbyid(this.id).subscribe(text => {
        console.log(text);
        this.stations = text;
      });
    }
    else if(this.type === 'pharmacie'){
      this.otherService.pharmaciebyid(this.id).subscribe(text => {
        console.log(text);
        this.pharmacies = text;
      });
    }
    else if(this.type === 'orange') {
      this.otherService.orangebyid(this.id).subscribe(text => {
        console.log(text);
        this.oranges = text;
      });
    }
    else if(this.type === 'signalisation'){
      this.otherService.signalisationbyid(this.id).subscribe(text => {
        console.log(text);
        this.signalisations = text;
      });
    }
    else if(this.type === 'hopital'){
      this.otherService.hopitalbyid(this.id).subscribe(text => {
        console.log(text);
        this.hopitals = text;
      });
    }  else if(this.type === 'gendarmerie'){
      this.otherService.gendarmeriebyid(this.id).subscribe(text => {
        console.log(text);
        this.gendarmeries = text;
      });
    }
    else if(this.type === 'vulcanisateur'){
      this.otherService.vulcanisateurbyid(this.id).subscribe(text => {
        console.log(text);
        this.vulcanisateurs = text;
      });
    }
    else if(this.type === 'arret' || this.type === 'arret2'){
      this.otherService.arretbyid(this.id).subscribe(text => {
        console.log(text);
        this.arrets = text;
      });

      this.otherService.linebyarretid(this.id).subscribe(lines =>{
        console.log(lines);
        this.lines = lines;
      });
    }
    else if (this.type === 'mecanicien'){
      this.mecanicienservice.mecanicienbyid(this.id).subscribe(text => {
        console.log(text);
        this.mecaniciens = text;
      });

      this.reviewService.myratingbymecanicien(this.id).subscribe(mynumber => {
        console.log(mynumber);
        this.starRating = mynumber;
      });
    }
  }

  gotodetail(id) {
    if (this.type === 'station'){
      const idstation = id;
      const navigationExtras: NavigationExtras = {
        state: {idstation}
      };
      this.dismiss();
      this.router.navigate(['/detailstation'], navigationExtras);
    }
    else if(this.type === 'pharmacie'){
      const idpharmacie = id;
      const navigationExtras: NavigationExtras = {
        state: {idpharmacie}
      };
      this.dismiss();
      this.router.navigate(['/detailpharmacie'], navigationExtras);
    }
      else if(this.type === 'vulcanisateur'){
      const idvulcanisateur = id;
      const navigationExtras: NavigationExtras = {
        state: {idvulcanisateur}
      };
      this.dismiss();
      this.router.navigate(['/detailvulcanisateur'], navigationExtras);
    }

      else if(this.type === 'hopital'){
      const idhopital = id;
      const navigationExtras: NavigationExtras = {
        state: {idhopital}
      };
      this.dismiss();
      this.router.navigate(['/detailhopital'], navigationExtras);
    }
  else if(this.type === 'gendarmerie'){
      const idgendarmerie = id;
      const navigationExtras: NavigationExtras = {
        state: {idgendarmerie}
      };
      this.dismiss();
      this.router.navigate(['/detailgendarmerie'], navigationExtras);
    }

    else if(this.type === 'signalisation'){
      const idsignalisation =id;
      const navigationExtras: NavigationExtras = {
        state: {idsignalisation}
      };
      this.dismiss();
      this.router.navigate(['/signalisationdetail'], navigationExtras);
    }
    else if (this.type === 'mecanicien'){
      const idmecanicien = id;
      const navigationExtras: NavigationExtras = {
        state: {idmecanicien}
      };
      this.dismiss();
      this.router.navigate(['/detailmecanicien'], navigationExtras);
    }

    else if (this.type === 'arret2'){
      const idline = id;
      const navigationExtras: NavigationExtras = {
        state: {idline}
      };
      this.dismiss();
      this.router.navigate(['/detailline'], navigationExtras);

    }

  }
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true
    });

  }


  async infopoparret(id,type) {
    this.dismiss();
    const signalerModal2 = await  this.modalController.create({component: DetailpopupPage,
      cssClass: 'cmodal',
      swipeToClose: true, componentProps: {
        id,type
      }

    });
    return await signalerModal2.present();

  }

}
