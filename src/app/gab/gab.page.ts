import { Component, OnInit } from '@angular/core';
import {Gaz} from '../model/gaz';
import {Router} from '@angular/router';
import {DetailstationService} from '../services/detailstation/detailstation.service';
import {Gab} from '../model/gab';

@Component({
  selector: 'app-gab',
  templateUrl: './gab.page.html',
  styleUrls: ['./gab.page.scss'],
})
export class GabPage implements OnInit {
  public id: number;
  public gab: Gab;

  constructor(private router: Router, private stationservice: DetailstationService) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      idgab: number;
    };
    if (state == null || state.idgab === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      this.id = state.idgab;
      console.log(this.id);
    }
  }

  ngOnInit() {
    this.detailgab();
  }
  detailgab(){
    this.stationservice.gabbyid(this.id).subscribe(text => {
      console.log(text);
      this.gab = text;
    });
  }
}
