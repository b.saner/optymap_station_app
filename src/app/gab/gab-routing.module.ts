import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GabPage } from './gab.page';

const routes: Routes = [
  {
    path: '',
    component: GabPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GabPageRoutingModule {}
