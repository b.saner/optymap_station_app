import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GabPageRoutingModule } from './gab-routing.module';

import { GabPage } from './gab.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GabPageRoutingModule
  ],
  declarations: [GabPage]
})
export class GabPageModule {}
