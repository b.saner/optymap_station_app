import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailpharmaciePage } from './detailpharmacie.page';

const routes: Routes = [
  {
    path: '',
    component: DetailpharmaciePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DetailpharmaciePageRoutingModule {}
