import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailpharmaciePageRoutingModule } from './detailpharmacie-routing.module';

import { DetailpharmaciePage } from './detailpharmacie.page';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailpharmaciePageRoutingModule,
    TranslateModule
  ],
  declarations: [DetailpharmaciePage]
})
export class DetailpharmaciePageModule {}
