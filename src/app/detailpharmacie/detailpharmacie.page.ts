import { Component, OnInit } from '@angular/core';
import Swal from "sweetalert2";
import {Mecanicien} from "../model/mecanicien";
import {Router} from '@angular/router';
import {CallNumber} from '@ionic-native/call-number/ngx';
import {ModalController} from "@ionic/angular";
import {ReviewService} from "../services/review/review.service";
import {MaponePage} from "../mapone/mapone.page";
import {Pharmacie} from "../model/pharmacie";
import {OtherService} from "../services/otherservices/other.service";
import {Manager} from '../model/manager';
import {RegionService} from '../services/region/region.service';
import {PopupcallPage} from "../popupcall/popupcall.page";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-detailpharmacie',
  templateUrl: './detailpharmacie.page.html',
  styleUrls: ['./detailpharmacie.page.scss'],
})
export class DetailpharmaciePage implements OnInit {

  public id: number;
  public pharmacies: Pharmacie;
  public departement: any;
  language: string;

  constructor(private router: Router, private callNumber: CallNumber, private otherService: OtherService,
              private modalController: ModalController,private regionservice: RegionService, private translateService: TranslateService
  ) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      idpharmacie: number;
    };
    if (state == null || state.idpharmacie === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      console.log(state);
      this.id = state.idpharmacie;
      this.pharmacies= new Pharmacie();
      this.pharmacies.manager= new Manager();

    }

  }

  ngOnInit() {
    if (localStorage.getItem('lang') ) {
      this.language = (localStorage.getItem('lang'));
      this.translateService.use(this.language);
    }
    this.detailpharmacie();
  }

  detailpharmacie(){
    this.otherService.pharmaciebyid(this.id).subscribe(text => {
      console.log(text);
      this.pharmacies = text;
      this.detaildepartement(text.departement);

    });
  }

  detaildepartement(id){
    console.log(id);
    this.regionservice.departmentbyid( parseInt(id)).subscribe(text => {
      console.log(text);
      this.departement = text;
    });
  }

  appel(numero) {
    if (numero === 'none'){
      Swal.fire({
        icon: 'error', title: 'Le numéro est indisponible', showConfirmButton: false, timer: 3000,
      });
    }
    else {

      this.callNumber.callNumber(numero, true)
        .then(res =>{ console.log('Launched dialer!', res);
          this.otherService.callclick(this.id,this.pharmacies).subscribe(call => {
            console.log(call);
            this.detailpharmacie();
          });})
        .catch(err => console.log('Error launching dialer', err));
    }
  }

  async emplacement(id) {
    const signalerModal = await this.modalController.create({
      component: MaponePage,
      cssClass: 'my-custom-class',
      swipeToClose: true, componentProps: {
        id
      }

    });
    return await signalerModal.present();

  }


  statutpharma(statut) {
    if (statut == 0){
      Swal.fire({
        icon: 'error', title: 'Le service est indisponible', showConfirmButton: false, timer: 3000,
      });
    }
    else if (statut == 3){
      Swal.fire({
        icon: 'warning', title: 'Aucune information sur ce service', showConfirmButton: false, timer: 3000,
      });
    }
    else if (statut == 2){
      Swal.fire({
        icon: 'error', title: 'Ce service est en panne', showConfirmButton: false, timer: 3000,
      });
    }
    else {
      Swal.fire({
        icon: 'success', title: 'Ce service est disponible', showConfirmButton: false, timer: 3000,
      });}
  }

  async infocall(tel: string) {
    if (tel == null || tel =='none' ||tel ==undefined){
      Swal.fire({
        icon: 'error', title: 'Le numéro est indisponible', showConfirmButton: false, timer: 2500,
      });
    }
    else{
      const callModal = await this.modalController.create({
        component: PopupcallPage,
        cssClass: 'cmodal',
        swipeToClose: true, componentProps: {
          tel
        }
      });
      return await callModal.present();
    }
  }

}
