import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PopupcallPage } from './popupcall.page';

const routes: Routes = [
  {
    path: '',
    component: PopupcallPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PopupcallPageRoutingModule {}
