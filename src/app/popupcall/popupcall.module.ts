import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PopupcallPageRoutingModule } from './popupcall-routing.module';

import { PopupcallPage } from './popupcall.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PopupcallPageRoutingModule
  ],
  declarations: [PopupcallPage]
})
export class PopupcallPageModule {}
