import {Component, Input, OnInit} from '@angular/core';
import Swal from 'sweetalert2';
import {CallNumber} from '@ionic-native/call-number/ngx';
import {ModalController, ToastController} from '@ionic/angular';
import { Clipboard } from '@ionic-native/clipboard/ngx';
import {CallServices} from '../services/call/call.service';
import {Callservice} from '../model/callservice';
import {Utilisateur} from '../model/utilisateur';
import {Typeservice} from '../model/typeservice';

@Component({
  selector: 'app-popupcall',
  templateUrl: './popupcall.page.html',
  styleUrls: ['./popupcall.page.scss'],
})
export class PopupcallPage implements OnInit {
  @Input() tel: string;
  @Input() idmecanicien: number;
  public callservice: Callservice;
  showpassword = false;
  passwordToggle = 'eye';

  constructor(private callNumber: CallNumber,  private callservices: CallServices,
              private modalController: ModalController, private clipboard: Clipboard,
              private toastController: ToastController) {
    this.callservice = new Callservice();
    this.callservice.user = new Utilisateur();
    this.callservice.typeservice = new Typeservice();

  }

  ngOnInit() {
    this.callservice.user.id = parseInt(localStorage.getItem('iduser'));
    this.callservice.typeservice.id = 2;
    this.callservice.serviceid = this.idmecanicien;
    console.log(this.idmecanicien);
  }

  async toastCopy() {
    const toast = await this.toastController.create({
      message: 'Le numéro a été copié',
      duration: 2500,
      position: 'bottom',
    });
    toast.present();
  }
  appel() {
    console.log(this.tel);
    if (this.tel === 'none') {
      Swal.fire({
        icon: 'error', title: 'Le numéro est indisponible', showConfirmButton: false, timer: 3000,
      });
    } else {
      this.callNumber.callNumber(this.tel, true)
        .then(res => console.log('Launched dialer!', res))
        .catch(err => console.log('Error launching dialer', err));
      this.callservices.callservice(this.callservice).subscribe(call => {
        console.log(call);
      });
    }
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true
    });

  }

  toggletel(): void {
    this.showpassword = !this.showpassword;
    if (this.passwordToggle === 'eye') {
      this.passwordToggle = 'eye-off';
    } else {
      this.passwordToggle = 'eye';
    }
  }

  copynumb(telephone){
    console.log(telephone)
    this.clipboard.copy(telephone);
    this.toastCopy();
  }
}
