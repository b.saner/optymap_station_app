import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {DetailstationService} from '../services/detailstation/detailstation.service';
import {Gazoil} from '../model/gazoil';

@Component({
  selector: 'app-gazoil',
  templateUrl: './gazoil.page.html',
  styleUrls: ['./gazoil.page.scss'],
})
export class GazoilPage implements OnInit {
  public id: number;
  private gazoil: Gazoil;

  constructor(private router: Router, private stationservice: DetailstationService) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      idgazoil: number;
  };
    if (state == null || state.idgazoil === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      this.id = state.idgazoil;
      console.log(this.id);
    }
  }

  ngOnInit() {
    this.detailgazoil();
  }
  detailgazoil(){
    this.stationservice.gazoilbyid(this.id).subscribe(text => {
      console.log(text);
      this.gazoil = text;
    });
  }
}
