import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GazoilPageRoutingModule } from './gazoil-routing.module';

import { GazoilPage } from './gazoil.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GazoilPageRoutingModule
  ],
  declarations: [GazoilPage]
})
export class GazoilPageModule {}
