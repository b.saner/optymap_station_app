import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GazoilPage } from './gazoil.page';

const routes: Routes = [
  {
    path: '',
    component: GazoilPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GazoilPageRoutingModule {}
