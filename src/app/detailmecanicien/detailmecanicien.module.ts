import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailmecanicienPageRoutingModule } from './detailmecanicien-routing.module';

import { DetailmecanicienPage } from './detailmecanicien.page';
import {StarRatingModule} from 'ionic5-star-rating';
import {TranslateModule} from "@ngx-translate/core";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailmecanicienPageRoutingModule,
    StarRatingModule,
    TranslateModule
  ],
  declarations: [DetailmecanicienPage]
})
export class DetailmecanicienPageModule {}
