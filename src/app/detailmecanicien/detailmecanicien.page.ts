import { Component, OnInit } from '@angular/core';
import {NavigationExtras, Router} from '@angular/router';
import {CallNumber} from '@ionic-native/call-number/ngx';
import {MecanicienService} from '../services/mecanicien/mecanicien.service';
import {Mecanicien} from '../model/mecanicien';
import Swal from 'sweetalert2';
import {ModalController} from '@ionic/angular';
import {MaponePage} from '../mapone/mapone.page';
import {async} from 'rxjs';
import {RatingPage} from '../rating/rating.page';
import {ReviewService} from '../services/review/review.service';
import {Review} from '../model/review';
import {Utilisateur} from '../model/utilisateur';
import {OtherService} from "../services/otherservices/other.service";
import {RegionService} from "../services/region/region.service";
import {PopupcallPage} from "../popupcall/popupcall.page";
import {Callservice} from "../model/callservice";
import {Typeservice} from "../model/typeservice";
import {CallServices} from "../services/call/call.service";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-detailmecanicien',
  templateUrl: './detailmecanicien.page.html',
  styleUrls: ['./detailmecanicien.page.scss'],
})
export class DetailmecanicienPage implements OnInit {
  public id: number;
  public mecaniciens: Mecanicien;
  public starRating = 0;
  public review2: Review;
  public callservice: Callservice;
  public anonyme = true;
  public departement: any;
  language: string;

  constructor(private router: Router, private callNumber: CallNumber, private mecanicienservice: MecanicienService,
              private callservices: CallServices,
              private regionservice: RegionService, private modalController: ModalController,
              private reviewService: ReviewService,private ratingService: ReviewService,
              private translateService: TranslateService
  ) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      idmecanicien: number;
    };
    if (state == null || state.idmecanicien === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      console.log(state);
      this.id = state.idmecanicien;
      console.log(this.starRating);
      this.mecaniciens= new Mecanicien();
      this.callservice = new Callservice();
      this.callservice.user = new Utilisateur();
      this.callservice.typeservice = new Typeservice();
      this.review2 = new Review();
      this.review2.reviewer = new Utilisateur();
      this.review2.mecanicien = new Mecanicien();
    }

    }

  ngOnInit() {
    if (localStorage.getItem('lang') ) {
      this.language = (localStorage.getItem('lang'));
      this.translateService.use(this.language);
    }
    this.detailmecanicien();
    this.myrating();
  this.review2.reviewer.id = parseInt(localStorage.getItem('iduser'));
  this.review2.mecanicien.id = this.id;

  this.callservice.user.id = parseInt(localStorage.getItem('iduser'));
  this.callservice.typeservice.id = 2;
  this.callservice.serviceid = this.id;
}

  detailmecanicien(){
    this.mecanicienservice.mecanicienbyid(this.id).subscribe(text => {
      console.log(text);
      this.mecaniciens = text;
      this.detaildepartement(text.departement);

    });
  }
detaildepartement(id){
  console.log(id);
    this.regionservice.departmentbyid( parseInt(id)).subscribe(text => {
      console.log(text);
      this.departement = text;
    });
  }

  myrating(){
  this.reviewService.myratingbymecanicien(this.id).subscribe(mynumber => {
    console.log(mynumber);
    this.starRating = mynumber;
  });
}

  appel(numero) {
    if (numero === 'none'){
      Swal.fire({
        icon: 'error', title: 'Le numéro est indisponible', showConfirmButton: false, timer: 3000,
      });
    }
    else {

      this.callNumber.callNumber(numero, true)
        .then(res =>{ console.log('Launched dialer!', res);
        this.callservices.callservice(this.callservice).subscribe(call => {
        console.log(call);
          this.detailmecanicien();
          this.myrating();
        });})
        .catch(err => console.log('Error launching dialer', err));
    }
  }

  async emplacement(id) {
    const signalerModal = await this.modalController.create({
      component: MaponePage,
      cssClass: 'my-custom-class',
      swipeToClose: true, componentProps: {
        id
      }

    });
    return await signalerModal.present();

  }

  async rating() {
    if (localStorage.getItem('typeuser') === '1') {
      Swal.fire({
        icon: 'warning',
        title: 'Vous ne pouvez pas noter un mécanicien sans vous inscrire',
        showConfirmButton: false,
        timer: 2500,
      });
    } else {
     const id = this.id;
      const ratingModal = await this.modalController.create({
            component: RatingPage,
            cssClass: 'my-custom-class',
            swipeToClose: true,
            componentProps: {
              id
            }
          });
      ratingModal.onDidDismiss().then((dismiss)=>{
        this.myrating();
      });
          return await ratingModal.present();

        }
      }

  statutmec(statut, type) {
    let typemecano;
    if (type == "mecanique"){
      typemecano = "mecanique";
    }
    else if (type == "carosserie"){
      typemecano = "carosserie";
    }
    else if (type == "electrique"){
      typemecano = "electrique";
    }

    else if (type == "depannage"){
      typemecano = "depannage";
    }
    else if (type == "remorquage"){
      typemecano = "remorquage";
    }
    else if (type == "vdiagnostic"){
      typemecano = "diagnostic";
    }
    if (statut == 0){

      Swal.fire({
        icon: 'error',title:typemecano, text: 'Le service est indisponible', showConfirmButton: false, timer: 3000,
      });
    }
    else if (statut == 3){
      Swal.fire({
        icon: 'warning', title:typemecano, text: 'Aucune information sur ce service', showConfirmButton: false, timer: 3000,
      });
    }
    else if (statut == 2){
      Swal.fire({
        icon: 'error', title:typemecano, text: 'Ce service est en panne', showConfirmButton: false, timer: 3000,
      });
    }
    else {
      Swal.fire({
        icon: 'success', title:typemecano, text: 'Ce service est disponible', showConfirmButton: false, timer: 3000,
      });}
  }

  ratingChange2(rating){
    console.log('changed rating: ', rating);
    this.starRating = rating;
    // do your stuff
  }

  onSubmit2() {
    console.log(this.anonyme);
    if (this.anonyme == true){
      this.review2.nom = 'anonyme';
    }
    else if(this.anonyme == false) {
      this.review2.nom = localStorage.getItem('nomuser');
    }
    else {
      this.review2.nom = 'anonyme';
    }
    // this.review.huissier.id = parseInt(localStorage.getItem('huissierid'));
    console.log(this.review2);
    this.review2.rating = this.starRating;
    this.ratingService.rating(this.review2).subscribe(infosignal => {
      console.log(infosignal);
      Swal.fire({
        icon: 'success',
        // title: 'Merci de votre signalisation',
        html: '<p style="font-size: 15px">Le mecanicien a été noté</p>',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
      });
      // Swal.fire({
      //     icon: 'warning',
      //     showCancelButton: true,
      //     // title: 'Enregistrement',
      //     html: '<p style="font-size: 15px">Pour une meilleur prise en charge, enregistrez vous</p>',
      //     showConfirmButton: false,
      //     timer: 3000,
      //     timerProgressBar: true,
      // }).then((result) => {
      //     if (result.isConfirmed) {
      //     this.router.navigate(['/register']);
      //     }
      // });
      // this.dismiss();
    }, error => {
      Swal.fire({
        icon: 'error',
        // title: 'Merci de votre signalisation',
        html: '<p style="font-size: 15px">Le mecanicien n\'a pas été noté</p>',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,

      });
    });
  }

  async infocall(tel: string) {
    if (tel == null || tel =='none' ||tel ==undefined){
      Swal.fire({
        icon: 'error', title: 'Le numéro est indisponible', showConfirmButton: false, timer: 2500,
      });
    }
    else{
      const callModal = await this.modalController.create({
        component: PopupcallPage,
        cssClass: 'cmodal',
        swipeToClose: true, componentProps: {
          tel,
          idmecanicien : this.id
        }
      });
      return await callModal.present();
    }
  }

}

