import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {DetailstationService} from '../services/detailstation/detailstation.service';
import {Entretien} from '../model/entretien';

@Component({
  selector: 'app-entretien',
  templateUrl: './entretien.page.html',
  styleUrls: ['./entretien.page.scss'],
})
export class EntretienPage implements OnInit {


  public id: number;
  public entretien: Entretien;

  constructor(private router: Router, private stationservice: DetailstationService) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      identretien: number;
    };
    if (state == null || state.identretien === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      this.id = state.identretien;
      console.log(this.id);
    }
  }

  ngOnInit() {
    this.detailentretien();
  }
  detailentretien(){
    this.stationservice.entretienbyid(this.id).subscribe(text => {
      console.log(text);
      this.entretien = text;
    });
  }
}
