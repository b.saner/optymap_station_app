import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.page.html',
  styleUrls: ['./setting.page.scss'],
})
export class SettingPage implements OnInit {
public mdsombre = false;
  language: string;
  mapsetting: string;
  constructor(private translateService: TranslateService) { }

  ngOnInit() {
   if (localStorage.getItem('mdsombre') === '1') {
     this.mdsombre = true;
     if (localStorage.getItem('lang') ) {
       this.language = (localStorage.getItem('lang'));
       this.translateService.use(this.language);
     }
     if (localStorage.getItem('mapsetting') ) {
       this.mapsetting = (localStorage.getItem('mapsetting'));
     }
   }


  }

  mdChange($event: CustomEvent) {
    localStorage.setItem('mdsombre',this.mdsombre === true? '1':'0');
if ($event.detail.checked){
  document.body.setAttribute('color-theme','dark');

}
else {
  document.body.setAttribute('color-theme','light');

}

  }



  changeLanguage() {
    localStorage.setItem('lang',this.language);
    this.translateService.use(this.language);
  }
  changeMapsetting() {
    localStorage.setItem('mapsetting',this.mapsetting);
  }
}
