import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddstationPage } from './addstation.page';

const routes: Routes = [
  {
    path: '',
    component: AddstationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddstationPageRoutingModule {}
