import { Component, OnInit, ViewChild } from '@angular/core';
import {ActionSheetController, IonSlides, Platform, ModalController} from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import Swal from 'sweetalert2';
import {NavigationExtras, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators, FormControl, FormArray} from '@angular/forms';
import {Station} from '../model/station';
import {RegionService} from '../services/region/region.service';
import {debounceTime, retry} from 'rxjs/operators';
import {Map, tileLayer, marker, icon, circle, polygon, geoJSON, Marker} from 'leaflet';
import * as L from 'leaflet';
import {fromEvent} from 'rxjs';
import {Electrique} from '../model/electrique';
import {Gpl} from '../model/gpl';
import {Essence} from '../model/essence';
import {Gazoil} from '../model/gazoil';
import {Superette} from '../model/superette';
import {Gaz} from '../model/gaz';
import {Entretien} from '../model/entretien';
import {Toilette} from '../model/toilette';
import {Manager} from '../model/manager';
import {Gab} from '../model/gab';
import {Airederepos} from '../model/airederepos';
import {Culte} from '../model/culte';
import {StationService} from '../services/station/station.service';

const meicon = L.icon({
  iconUrl: '../assets/icon/station.png',
  iconSize: [35, 41],
  iconAnchor: [10, 41],
  popupAnchor: [2, -45], });

@Component({
  selector: 'app-addstation',
  templateUrl: './addstation.page.html',
  styleUrls: ['./addstation.page.scss'],
})
export class AddstationPage implements OnInit {
  @ViewChild('slides', {static: true}) slides: IonSlides;
  public slideNameForm: FormGroup;
  public slideRegionForm: FormGroup;
  public slideOneForm: FormGroup;
  public slideGazForm: FormGroup;
  public slideGabForm: FormGroup;
  public slidePositionForm: FormGroup;
  public slideTwoForm: FormGroup;
  public slideCommentaireForm: FormGroup;
  public slideAlimentationForm: FormGroup;
  public slideEntretienForm: FormGroup;
  public slideCarburantForm: FormGroup;
  public slideReposForm: FormGroup;
  public slideCulteForm: FormGroup;
  public station: Station;
  public mylatitude: any;
  public mylongitude: any;
  public type: number;
  public motif: string;
  public map: Map;
  newMarker: any;
  public latlong: any;
  departements: any;
  regions: any;
  slideOpts = {
    initialSlide: 0,
  };
  photocheck: string;

  existgab: any;
  nombregab: any;
  existgaz: any;


  constructor(private modalController: ModalController, public formBuilder: FormBuilder, private router: Router,
              private geolocation: Geolocation, private actionSheetController: ActionSheetController,
            private regionService: RegionService,private stationService: StationService) {
    this.slideNameForm = formBuilder.group({
      name: [''],
      pmanager: [''],
      emanager: [''],
      nmanager: [''],
      tmanager: [''],
    });

    this.slideRegionForm = formBuilder.group({
      region: [''],
      departement: [''],
      localite: [''],
    });

    this.slideReposForm = formBuilder.group({
      statut: [''],
      airedepicnic: [''],
      espaceenfant: [''],
      eau: [''],
      gonflage: [''],
      handicape: [''],
      pvl: [''],
      ppl: [''],
      ppc: [''],
    });

    this.slideGazForm = formBuilder.group({
      statut: [''],
    });

    this.slidePositionForm = formBuilder.group({
    });
    this.slideAlimentationForm = formBuilder.group({
      toilette: [''],
      superette: [''],
      fastfood: [''],
      nomfastfood: [''],
      boulangerie: [''],
      patisserie: [''],
      nomrestaurant: [''],
      restaurant: [''],
      cafeteria: [''],
      espacebebe: [''],
      wch: [''],
      wcf: [''],
      wcu: [''],
      name: [''],
      statut: [''],

      tmoney: [''],
      pmoney: [''],
      cmoney: [''],
      senelec: [''],
      seneau: [''],
      sonatel: [''],
      orange: [''],
      orangemoney: [''],
      yup: [''],
      canal: [''],
      postemoney: [''],
      woyofal: [''],
      wizall: [''],
      westerunion: [''],
      wave: [''],
      emoney: [''],
      freemoney: [''],
      wari: [''],
      proximo: [''],
      rapido: [''],
      ria: [''],

      cash: [''],
      carte: [''],
      kalpe: [''],
      ctotal: [''],
      celton: [''],

    });

    this.slideCarburantForm = formBuilder.group({
      gpl: [''],
      super: [''],
      gazoil: [''],
      electrique: [''],
      essence: [''],
      prixelec: [''],
      prixgaz: [''],
      prixgpl: [''],
      prixes: [''],

    });

    this.slideGabForm = formBuilder.group({
      existgab: [''],
      nombregab: [''],
      statut: [''],
      gabbanque: new FormArray([]),

    });

 this.slideCulteForm = formBuilder.group({

   statut: [''],
   mosque: [''],
   abulution: [''],
   eglise: [''],

    });

    this.slideGazForm = formBuilder.group({
      statut: [''],
      statut27: [''],
      statut6: [''],
      statut9: [''],
      statut125: [''],
      statut32: [''],
      statut38: [''],
      prix27: [''],
      prix6: [''],
      prix9: [''],
      prix125: [''],
      prix32: [''],
      prix38: [''],

    });
    this.slideEntretienForm = formBuilder.group({
      statut: [''],
      parebrise: [''],
      ventepneu: [''],
      frein: [''],
      climatisation: [''],
      lavage: [''],
      vidange: [''],
      mecanique: [''],
    });

  }

  ngOnInit() {
    this.station = new Station();
    this.station.electrique = new Electrique();
    this.station.gpl = new Gpl();
    this.station.essence = new Essence();
    this.station.gazoil = new Gazoil();
    this.station.superette = new Superette();
    this.station.gaz = new Gaz();
    this.station.gab = new Gab();
    this.station.airederepos = new Airederepos();
    this.station.entretien = new Entretien();
    this.station.culte = new Culte();
    this.station.toilette = new Toilette();
    this.station.manager = new Manager();
    this.listregion();
    this.slides.lockSwipes(true);

  }
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true
    });
  }

  // convenience getters for easy access to form fields dynamics gab
  get f() { return this.slideGabForm.controls; }
  get t() { return this.f.gabbanque as FormArray; }

  onChangeGab(e) {
    const numberOfGabs = e.target.value || 0;
    if (this.t.length < numberOfGabs) {
      for (let i = this.t.length; i < numberOfGabs; i++) {
        this.t.push(this.formBuilder.group({
          nom: [''],
          statut: ['']
        }));
      }
    } else {
      for (let i = this.t.length; i >= numberOfGabs; i--) {
        this.t.removeAt(i);
      }
    }
  }
  next() {
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true);
  }

  previous() {
    this.slides.lockSwipes(false);
    this.slides.slidePrev();
    this.slides.lockSwipes(true);
  }

  locatePosition() {
    this.map.locate({ watch: false, setView: true, maxZoom: 15, enableHighAccuracy : true}).on('locationfound', (e: any) => {
      console.log('location', e);
      this.latlong = e.latlng;
      // Si la localisation est disponible on integrera notre position
      this.newMarker = marker([e.latitude, e.longitude], {
        draggable: true,
        icon: meicon,
      })
      //   .on('click', event =>  {
      //   this.router.navigate(['/detailstation'], navigationExtras);
      // })
        .addTo(this.map);
      this.newMarker.on('dragend', f => {
        this.station.latitude = f.target.getLatLng().lat.toString();
        this.station.longitude = f.target.getLatLng().lng.toString();
      });

      // this.newMarker.bindPopup('<p>Ma position</p>');
      // this.newMarker2 = marker([e.latitude, e.longitude], animatedCircleIcon).addTo(this.map);
      // });
this.station.latitude = e.latitude;
this.station.longitude = e.longitude;
    });
  }

  ionViewDidEnter() {
    this.map = new Map('map3');
    tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',

    ).addTo(this.map);
    // }
    this.locatePosition();
  }

listregion(){
  this.regionService.allregion()
    .pipe( retry(1))
    .subscribe(res => {
      this.regions = res;
      console.log(res);
    }, error => {
      console.log(error);
    });
}
departementid(id){
  this.regionService.departmentbyregion(id)
    .pipe( retry(1))
    .subscribe(res => {
      this.departements = res;
      console.log(res);
    }, error => {
      console.log(error);
    });
}

ionViewDidLeave() {
    this.map.remove();
  }

  onSubmit() {
    this.station.gab.gabbanque = this.t.value;
    this.stationService.addstation(this.station).subscribe(station => {
      console.log(station);
      Swal.fire({
        icon: 'success', title: 'Votre station a été ajouter et ai en attente de validation', showConfirmButton: false, timer: 3000,
      });
      console.log(this.station);
    }, error => {
      Swal.fire({
        icon: 'error', title: 'Une erreur c\'est produit lors de l\'ajout !', showConfirmButton: false, timer: 3000,
      });    });
    this.router.navigate(['/home']);
  }
}
