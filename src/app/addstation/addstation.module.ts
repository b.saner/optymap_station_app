import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddstationPageRoutingModule } from './addstation-routing.module';

import { AddstationPage } from './addstation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddstationPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [AddstationPage]
})
export class AddstationPageModule {}
