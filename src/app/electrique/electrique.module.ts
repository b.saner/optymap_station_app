import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ElectriquePageRoutingModule } from './electrique-routing.module';

import { ElectriquePage } from './electrique.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ElectriquePageRoutingModule
  ],
  declarations: [ElectriquePage]
})
export class ElectriquePageModule {}
