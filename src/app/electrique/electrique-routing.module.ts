import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ElectriquePage } from './electrique.page';

const routes: Routes = [
  {
    path: '',
    component: ElectriquePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ElectriquePageRoutingModule {}
