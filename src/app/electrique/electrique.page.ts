import { Component, OnInit } from '@angular/core';
import {Essence} from '../model/essence';
import {Router} from '@angular/router';
import {DetailstationService} from '../services/detailstation/detailstation.service';
import {Electrique} from '../model/electrique';

@Component({
  selector: 'app-electrique',
  templateUrl: './electrique.page.html',
  styleUrls: ['./electrique.page.scss'],
})
export class ElectriquePage implements OnInit {

  public id: number;
  public electrique: Electrique;

  constructor(private router: Router, private stationservice: DetailstationService) {
    const navigation = this.router.getCurrentNavigation();
    const state = navigation.extras.state as {
      idelectrique: number;
    };
    if (state == null || state.idelectrique === undefined) {
      this.router.navigate(['home']);
      console.log(this.id);
    } else {
      this.id = state.idelectrique;
      console.log(this.id);
    }
  }

  ngOnInit() {
    this.detailelectrique();
  }
  detailelectrique(){
    this.stationservice.electriquebyid(this.id).subscribe(text => {
      console.log(text);
      this.electrique = text;
    });
  }
}
