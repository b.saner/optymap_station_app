import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../services/user/user.service';
import {RegionService} from '../services/region/region.service';
import {Utilisateur} from '../model/utilisateur';
import {retry} from 'rxjs/operators';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  public registerForm: FormGroup;
  public loading = false;
  showpassword = false;
  passwordToggle = 'eye';
  public submitted = false;
  public utilisateur: Utilisateur = new Utilisateur();
  departements: any;
  regions: any;
  public userid: string = localStorage.getItem('iduser');
  tooptp = true;
  otp: string;
  config = {
    allowNumbersOnly: false,
    length: 5,
    isPasswordInput: false,
    disableAutoFocus: false,
    placeholder: '',
    inputStyles: {
      'width': '15vw',
      'height': '50px'
    }
  };
  config2 = {
    leftTime: 240,
  };
  constructor(private formBuilder: FormBuilder, private router: Router, private authService: UserService,
              private regionService: RegionService) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      telephone: ['', Validators.required],
      password: ['', Validators.required],
      sexe: ['', Validators.required],
      prenom: ['', Validators.required],
      nom: ['', Validators.required],
      // departement: ['', Validators.required],
      // region: ['', Validators.required],
    });
    this.listregion();
  }

  togglepassword(): void {
    this.showpassword = !this.showpassword;
    if (this.passwordToggle === 'eye')  {
      this.passwordToggle = 'eye-off';
    } else {
      this.passwordToggle = 'eye';
    }
  }
  get f() {
    return this.registerForm.controls;
  }
  onSubmit() {
    this.loading = true;
    this.utilisateur.type = 2;
    console.log(this.utilisateur);
    this.authService.update(this.utilisateur, this.userid)
      .pipe( retry(1))
      .subscribe(res => {
        Swal.fire({
          icon: 'success', title: 'Votre enregistrement c\'est bien passé  ', showConfirmButton: false, timer: 2500,
        });
        localStorage.setItem('nomuser', this.f.prenom.value+' '+this.f.nom.value);
        localStorage.setItem('telephone', this.f.telephone.value);
        localStorage.setItem('typeuser', '2');
        this.loading = false;
        // this.router.navigate(['/login']);
        this.tooptp = !this.tooptp;

      }, error => {
        this.loading = false;
        Swal.fire({
          icon: 'error', title: 'Votre enregistrement rencontre une erreur', showConfirmButton: false, timer: 2500,
        });
      });

  }

  listregion(){
    this.regionService.allregion()
      .pipe( retry(1))
      .subscribe(res => {
        this.regions = res;
        console.log(res);
      }, error => {
        console.log(error);
      });
  }

  departementid(id){
      this.regionService.departmentbyregion(id)
      .pipe( retry(1))
      .subscribe(res => {
        this.departements = res;
        console.log(res);
      }, error => {
        console.log(error);
      });
  }

  gotologin(){
    this.router.navigate(['/login']);
  }

  onOtpChange(otp) {
    if (otp.length == 5){
      this.authService.valide(otp).subscribe((res => {
        console.log(res);}))
      this.otp = otp;
    console.log("5 atteint");
  }
  }

  handleEvent(event) {
    console.log(event);
    if (event.action == "done")
    {
      console.log("donne est fait");
      (document.querySelector('.countdownotp') as HTMLElement).style.display = 'none';
      (document.querySelector('.countdownrepeat') as HTMLElement).style.display = 'block';

    }  else if (event.action == "restart")
    {
      console.log("donne est fait");
      (document.querySelector('.countdownotp') as HTMLElement).style.display = 'block';
      (document.querySelector('.countdownrepeat') as HTMLElement).style.display = 'none';

    }
  }
}
